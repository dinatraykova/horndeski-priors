#!/usr/bin/python2.7
from plot_lib import Plot_figures
from work_in_class import wicmath
import numpy as np
import os
import sys

################################################################################
# Variables to modify
################################################################################

no_obs = False # Analyze coeffs from fit w/o obs

show = False  # If False, just print to file.

header_row = 1  # The row with the column names

histlog = False # Take log(coefficients)
histlogcomplex = False # Take log_modulus (raw coefficients)
histlogauto = True # Prevent plot_lib using histlog if detected bin with P(bin) > 0.8
log_hist_threshold = 0.5 # If histlogauto=True, change scale for hists with
                         # P(bin) > log_hist_threshold

reldev_log_hist_threshold = 0.5  # Change the max size for a bin in rel. dev. vs eigh
                          # histograms before changing from log10(rel. dev) to
                          # rel. dev_ Use 0 for linearizing all of them or sth
                          # larger than 1 to use log10.

densitylog = 'linear' # Take logscale in density hist.

nbins = 100  # Bins for raw histograms

xlimRes = [1, 1e7]  # xlim for residuals

compare_with = ''

basis_cut = 1e-2  # Keep minimum dimensions that yield rel. err. in observables < basis_cut
acceptance = 1e-3 # Number of parameters allowed to be over the basis_cut

redshift_to_compare = np.array([0.1, 0.5, 1, 10])


points_in_H_DA_f_plot = 20000      # If larger than the number of computed models,
                                   # it will just plot as much as computed models are.

store_intermediate_computations = False
save_final_computations = True
multiprocess = True
chunksize = 100

transform = False
run_residuals = True
only_residuals = False
run_plot_correlation_matrix = True
run_raw_histograms = True
run_rebinned_histograms = True
run_diagonalized_histograms = True
run_check_dim_reduction = True
run_diagonal_basis_evolution = True
run_fitted_function_eigenvalues = True
run_relative_error_dimensions = True
run_check_dim_reduction_sampling = True
run_plot_w0wa_fit = True
run_plot_H_DA_f = True

remove_outliers_sigma = 0
remove_out_of_bounds = False
remove_crazy_functions = False

fig_output_format = 'pdf'

w0wafit_zmax = 2

index_function_eigenvalues = 14100

def select_crazy_functions(functions):
    return functions[:, 0] < 0

def transform_function(data):
    return np.sign(data) * np.array(np.abs(data)) ** (1./5.)

def transform_function_inverse(data):
    return np.sign(data) * np.array(np.abs(data)) ** (5.)

transform_function_label = 'pow[1/5]'

def main(preroot, fit_function=None, time_variable=None, no_obs=False):
    ################################################################################
    # Initialize the Plot_figures class
    ################################################################################

    plotfi = Plot_figures(preroot, fit_function, time_variable, no_obs=no_obs, show=show, header_row=header_row,
                          histlog=histlog, histlogcomplex=histlogcomplex,
                          nbins=nbins, basis_cut=basis_cut,
                          points_in_H_DA_f_plot=points_in_H_DA_f_plot,
                          redshift_to_compare=redshift_to_compare,
                          remove_outliers_sigma=remove_outliers_sigma,
                          fig_output_format=fig_output_format,
                          densitylog=densitylog, histlogauto=histlogauto,
                          remove_out_of_bounds=remove_out_of_bounds,
                          log_hist_threshold=log_hist_threshold,
                          store_intermediate_computations=store_intermediate_computations,
                          save_final_computations=save_final_computations,
                          multiprocess=multiprocess,
                          only_residuals=only_residuals, acceptance=acceptance,
                          chunksize=chunksize)

    ################################################################################
    # Plot residuals
    ################################################################################

    if run_residuals:
        plotfi.plot_residuals()

    if only_residuals:
        return

    #########
    # Plot correlation matrix
    #########

    if run_plot_correlation_matrix:
        plotfi.hist_orig_scale_orig.plot_correlation_matrix(xlabel='fit-w coeff',
                                                            ylabel='fit-w coeff',
                                                            bins_step=2,
                                                            title='Correlation matrix. ' + plotfi.model_title,
                                                            outpath=os.path.join(plotfi.outdir_figs,
                                                                                 plotfi.modelname + '-correlation-matrix.pdf'),
                                                            show=show)

        if plotfi.hist_orig_scale_orig is not plotfi.hist_corrected_scale_orig:
            plotfi.hist_corrected_scale_orig.plot_correlation_matrix(xlabel='fit-w coeff',
                                                                     ylabel='fit-w coeff',
                                                                     bins_step=2,
                                                                     title='Correlation matrix. ' + plotfi.model_title,
                                                                     outpath=os.path.join(plotfi.outdir_figs,
                                                                                          plotfi.modelname + '-correlation-matrix_corrected-scale.pdf'),
                                                                     show=show)


    #########
    # Plot histograms
    #########

    if run_raw_histograms:
        plotfi.plot_histograms(run_raw_histograms, run_rebinned_histograms,
                               run_diagonalized_histograms)

    #########
    # Plot diagonal covariance matrix
    #########

    if run_diagonalized_histograms:
        plotfi.hist_diagonal_orig.plot_covariance_matrix(xlabel='Diag. coeff.',
                                                         ylabel='Diag. coeff.',
                                                         bins_step=2,
                                                         title='Covariance matrix. ' + plotfi.model_title,
                                                         outpath=os.path.join(plotfi.outdir_figs,
                                                                              plotfi.modelname + '-covariance-matrix-diagonalized_corrected_scale.pdf'),
                                                         show=show)

    ################
    # Plot diagonal basis time evolution.
    ################
    if run_diagonal_basis_evolution:
        plotfi.plot_diagonal_basis_time_evolution()
    ################
    # Plot fitted_function with diff. number of eigenvalues
    ################
    if run_fitted_function_eigenvalues:
        try:
            plotfi.plot_fitted_function_from_reduced_space(index_function_eigenvalues)
        except IndexError:
            sys.stdout.write('Changing index_function_eigenvalues to -1. It was out of bounds')
            plotfi.plot_fitted_function_from_reduced_space(-1)

    ################
    # Plot relative error vs number of dimensions.
    ################
    if run_relative_error_dimensions:
        plotfi.plot_relative_error_vs_dimensions_fitted_function(log_hist_threshold=reldev_log_hist_threshold)
        plotfi.plot_relative_error_vs_dimensions_observables(log_hist_threshold=reldev_log_hist_threshold)

    ######################################################################
    # Plot histograms comparison between original basis and recovered from
    # dimensional reduction.
    ######################################################################

    if run_check_dim_reduction:
        plotfi.plot_check_dimensional_reduction()

    ################
    # Plot histograms comparison between original and recovered from dimensional
    # reduction after sampling in the reduced diagonalized basis.
    ################
    if run_check_dim_reduction_sampling:
        plotfi.hist_diagonal_sampled.plot_covariance_matrix(xlabel='Diag. coeff.',
                                                         ylabel='Diag. coeff.',
                                                         bins_step=2,
                                                         title='Covariance matrix. ' + plotfi.model_title,
                                                         outpath=os.path.join(plotfi.outdir_figs,
                                                                              plotfi.modelname + '-covariance-matrix-diagonalized_corrected_scale_sampled.pdf'),
                                                         show=show)

        plotfi.plot_check_sampling_histograms()

        plotfi.plot_check_dimensional_reduction_sampling()

        ################
        # Plot fitted functions comparison between original and sampled
        ################
        plotfi.plot_fitted_functions_original_and_sampled()

        ################
        # Plot w evolution comparison between original and sampled (default option)
        ################
        plotfi.plot_w_evolution_original_and_sampled(n=1000)

        ################
        # Plot w0-wa comparison between original and sampled (default options)
        ################
        try:
            plotfi.plot_w0wa_contours_original_and_sampled(num_contours=2,
                                                           alpha=0.4,
                                                           smooth_scale_2D=-1,
                                                           ranges=True)
        except Exception as e:
            # Added because it failed for axions.
            sys.stderr.write('Error in w0wa: ' + str(e))

        ################
        # Plot w0-wa fitted to logX comparison between original and sampled (default options)
        ################
        if run_plot_w0wa_fit:
            plotfi.plot_w0wa_fit_histograms()
            try:
                plotfi.plot_w0wa_fit_contours_original_and_sampled(num_contours=2,
                                                                   alpha=0.4,
                                                                   smooth_scale_2D=-1,
                                                                   ranges=True)
            except Exception as e:
                if 'Contour level outside plotted ranges' in str(e):
                    try:
                        plotfi.plot_w0wa_fit_contours_original_and_sampled(num_contours=3,
                                                                           alpha=0.4,
                                                                           smooth_scale_2D=-1,
                                                                           ranges=False)
                    except Exception as e:
                        sys.stderr.write('Error in w0wa-fitted: ' + str(e))

                else:
                    sys.stderr.write('Error in w0wa-fitted: ' + str(e))

        ################
        # Plot f-D_A comparison between original and sampled
        ################
        if run_plot_H_DA_f:
            try:
                plotfi.plot_H_DA_f_original_and_sampled(num_contours=2, alpha=0.4,
                                                        smooth_scale_2D=-1)
            except Exception as e:
                sys.stderr.write('Error in H_DA_f: ' + str(e))

            try:
                plotfi.plot_DArec_original_and_sampled()
            except Exception as e:
                sys.stderr.write('Error in H_DA_f: ' + str(e))


if __name__ == '__main__':

    preroot = sys.argv[1]

   # try:
   #     time_array, z = get_xvar_and_z(sys.argv[2])
   # except IndexError:
   #     time_array, z = get_xvar_and_z(preroot)

   # try:
   #     fit_function = get_fit_function(sys.argv[3])
   # except IndexError:
   #     fit_function = get_fit_function(preroot)

    main(preroot, no_obs=no_obs) #, fit_function, z, time_array)
