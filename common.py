#!/usr/bin/python
from classy import Class
from scipy.interpolate import interp1d
import scipy.integrate as integrate
import work_in_class.cosmo_extra as cosmo_extra
import work_in_class.wicmath as wicm
import numpy as np

def class_params_CPL_fld(coeffs, class_params):
    if len(coeffs) == 1:
        coeffs = np.array([coeffs[0], 0])

    params = class_params.copy()
    del(params['Omega_smg'])
    del(params['gravity_model'])
    del(params['parameters_smg'])
    del(params['Omega_fld'])
    if 'parameters_2_smg' in params.keys():
        del(params['parameters_2_smg'])

    params_fld = \
        {"Omega_Lambda": 0,
        "Omega_scf": 0,
        # Omega_fld unespecified

        "use_ppf": "yes",

        # "fluid_equation_of_state": "CLP",  # For a newer version of CLASS
        "cs2_fld": 1,

        'w0_fld': coeffs[0],
        'wa_fld': coeffs[1],
        }
    params.update(params_fld)
    return params

def class_params_w_expansion_fld(coeffs, class_params):
    params = class_params.copy()
    del(params['Omega_smg'])
    del(params['gravity_model'])
    del(params['parameters_smg'])
    del(params['Omega_fld'])
    if 'parameters_2_smg' in params.keys():
        del(params['parameters_2_smg'])

    params_fld = \
        {"Omega_Lambda": 0,
        "Omega_scf": 0,
        # Omega_fld unespecified

        "use_ppf": "yes",

        # "fluid_equation_of_state": "CLP",  # For a newer version of CLASS
        "cs2_fld": 1,

        'w0_fld': -100,
        'wa_fld': len(coeffs),
        'parameters_smg': str(list(coeffs)).strip('[]'),
        }

    params.update(params_fld)
    return params

def w0wa_function(xdata, coeffs):
    return coeffs[0] + coeffs[1] * xdata

def compute_fit_w0wa(z, rhoM, rhoR, rhoDE_0, H, DA, f, w, index_to_compare=None, rerr=1e-3, basis='(1-a)'):
    # Note that lna is log(1+z). I used this name because is more convenient
    if index_to_compare is None:
        index_to_compare = np.arange(len(z))

    # lna = np.log(z+1)
    if basis == 'lna':
        tvar = np.log(z+1)
    elif basis == '(1-a)':
        tvar = z/(z+1)

    z_to_compare = z[index_to_compare]

    # Prepare observables we are going to compare.
    dummy_X_input = np.ones(len(z_to_compare) * 3)
    obs_to_compare = np.concatenate([H[index_to_compare], DA[index_to_compare], f[index_to_compare]])
    obs_to_compare[obs_to_compare == 0] = 1e-100

    sigma = np.ones(obs_to_compare.shape[0]) * rerr

    ##############################################################
    ##############################################################
    ##############################################################
    def get_rhoDE_wfit_from_w0wa(tvar, coeffs):
        wfit = coeffs[0] + coeffs[1] * tvar
        if basis == 'lna':
            logX = 3 * ((1+coeffs[0]) * tvar + 0.5 * coeffs[1] * tvar**2.) # lna == ln(1+z) !
        elif basis == '(1-a)':
            logX = - 3 * ((sum(coeffs) + 1) * np.log(1 - tvar) + coeffs[1] * tvar)

        rhoDE_fit = rhoDE_0 * np.exp(logX)

        return rhoDE_fit, wfit

    def w0wa_function(xdata, coeffs):
        return coeffs[0] + coeffs[1] * xdata

    def observables_from_fit_coeffs(xdata, coeffs, to_compare=True):
        rhoDE_fit, w_fit = get_rhoDE_wfit_from_w0wa(tvar, coeffs)

        if to_compare:
            indexes = index_to_compare
        else:
            indexes = np.arange(len(z))

        obs, rec = \
            compute_observables_fit(z, rhoM, rhoR, rhoDE_fit,
                                                   w_fit, f[0], indexes,
                                                   z_to_compare[0])
        _, H_fit, DA_fit, f_fit = obs
        H_fit[~np.isfinite(H_fit)] = 1e30 * np.max(H_fit[np.isfinite(H_fit)])
        f_fit[np.isnan(f_fit)] = 1e4 # np.inf

        # Change obsFit's 0's to 1e-100 as in obs_to_compare
        obsFit = np.concatenate([H_fit, DA_fit, f_fit])
        obsFit[obsFit == 0] = 1e-100

        return obsFit

    ##############################################################
    ##############################################################
    ##############################################################

    # Fit Y first, to obtain a first value for p0
    # Get array of function to fit
    ###############################
    Y = w
    p0, _ =  wicm.fit(w0wa_function, tvar, Y, 2, ftol=1e-3, gtol=1e-3, xtol=1e-3)

    popt, _ = wicm.fit(observables_from_fit_coeffs, dummy_X_input,
                       obs_to_compare, 2,
                       p0=p0, sigma=obs_to_compare*sigma)


    return popt, p0

def compute_fit_w0wa_class(z, H, DA, f, w, cosmo_pars_smg, index_to_compare=None, rerr=1e-3, basis='(1-a)'):
    # Note that lna is log(1+z). I used this name because is more convenient
    if index_to_compare is None:
        index_to_compare = np.arange(len(z))

    z_to_compare = z[index_to_compare]
    z_low_redshift_fit = z_to_compare[0]

    if basis == '(1-a)':
        tvar = z/(z+1)
    else:
        raise ValueError('The only basis implemented in CLASS is (1-a)')
    # if basis == 'lna':
    #     tvar = np.log(z+1)

    # Prepare observables we are going to compare.
    dummy_X_input = np.ones(len(z_to_compare) * 3)
    obs_to_compare = np.concatenate([H[index_to_compare], DA[index_to_compare], f[index_to_compare]])
    obs_to_compare[obs_to_compare == 0] = 1e-100

    sigma = np.ones(obs_to_compare.shape[0]) * rerr

    ##############################################################
    ##############################################################
    ##############################################################
    cosmo = Class()
    def observables_from_fit_coeffs(xdata, coeffs, to_compare=True):
        class_params = class_params_CPL_fld(coeffs, cosmo_pars_smg)
        cosmo.set(class_params)

        if to_compare:
            teval = z_to_compare
        else:
            teval = z

        try:
            cosmo.compute()
            obs, rec = compute_observables_th(cosmo, z_low_redshift_fit, teval, fclass=True) # , f[0])
        except Exception as e:
            print(str(e))
            obs = np.ones((4, teval.size)) * 1e100
            rec = np.ones(2) * 1e100

        cosmo.struct_cleanup()

        _, H_fit, DA_fit, f_fit = obs
        _, DA_rec = rec
        # There are rhoDE == np.inf as some parameters make the parametrization go crazy.
        # We change np.inf to huge numbers.
        H_fit[~np.isfinite(H_fit)] = 1e30 * np.max(H_fit[np.isfinite(H_fit)])
        f_fit[np.isnan(f_fit)] = 1e4 # np.inf
        obsFit = np.concatenate([H_fit, DA_fit, f_fit]) # , [DA_rec]])
        obsFit[obsFit == 0] = 1e-100

        # print H_fit
        # print DA_fit
        # print f_fit
        # print DA_rec

        # print('chi2', np.sum(((obs_to_compare-obsFit) / (obs_to_compare * sigma))**2.))
        return obsFit

    ##############################################################
    ##############################################################
    ##############################################################

    # Fit Y first, to obtain a first value for p0
    # Get array of function to fit
    ###############################
    Y = w
    p0, _ =  wicm.fit(w0wa_function, tvar, Y, 2, ftol=1e-3, gtol=1e-3, xtol=1e-3)

    popt, _ = wicm.fit(observables_from_fit_coeffs, dummy_X_input,
                       obs_to_compare, 2,
                       p0=p0, sigma=obs_to_compare*sigma)


    return popt, p0, cosmo.pars

def compute_observables_fit(z, rhoM, rhoR, rhoDE_fit, w_fit, f0, index_to_compare=None, z_rec=None):
    """
    z > z_rec
    z[index_to_compare] < z_rec
    f0 = f_class(z = z[index_to_compare][-1])
    rho_i = rho_i(real) / 3. Use the same convention as CLASS for rho's
    """
    if index_to_compare is None:
        index_to_compare = np.arange(z.shape[0])
    if z_rec is None:
        z_rec = z[0]

    Z = z[index_to_compare]

    # Compute H for fitted model
    ################
    H_fit = np.sqrt(rhoM + rhoR + rhoDE_fit)

    # Compute the growth rate for fitted model
    ###############
    #OmegaMF_fit = interp1d(z, 1-rhoDE_fit/H_fit**2-rhoR/H_fit**2)   ####### THIS FITS OBSERVABLES CORRECTLY
    ## OmegaMF_fit = interp1d(z, rhoM/H_fit**2)      ####### THIS FITS OBSERVABLES CORRECTLY
    #OmegaDEwF_fit = interp1d(z, rhoDE_fit/H_fit**2 * w_fit)

    if len(Z) == 0:
        f_fit = np.array([])
    elif len(Z) == 1:
        f_fit = np.array([f0])
    else:
        OmegaMF_fit = interp1d(z[z <= Z[0]], (1-rhoDE_fit/H_fit**2-rhoR/H_fit**2)[z <= Z[0]])  ####### THIS FITS OBSERVABLES CORRECTLY
        OmegaDEwF_fit = interp1d(z[z <= Z[0]], (rhoDE_fit/H_fit**2 * w_fit)[z <= Z[0]])

        f_fit = integrate.solve_ivp(cosmo_extra.fprime(OmegaDEwF_fit, OmegaMF_fit), [Z[0], Z[-1]], [f0],
                                    method='LSODA', dense_output=False, t_eval=Z, min_step=1e-16).y[0]
        if len(f_fit) != len(Z):
            raise ValueError('We could not solve the differential equation for f!')

    Hinv_interp = interp1d(z, 1./H_fit)  # faster if we just interp1d z[z<=z_rec + 100]?
    DA = angular_distance(Hinv_interp, np.concatenate([Z, [z_rec]]))
    DA_fit = DA[:-1]
    DA_rec = DA[-1]
    H_fit = H_fit[index_to_compare]

    return np.array([Z, H_fit, DA_fit, f_fit]), np.array([z_rec, DA_rec])

def compute_observables_th(cosmo, z_low_redshift, z_output=None, f0=None, fclass=True):
    """
    z_output has to be sorted and max(z_output) <= z_low_redshift
    """
    b = cosmo.get_background()

    derived = cosmo.get_current_derived_parameters(['z_rec', 'da_rec'])
    z_rec = derived['z_rec']
    DA_rec = derived['da_rec']

    zlim = z_low_redshift
    zFull = b['z']

    sel = (zFull <= zlim)
    if z_output is not None:
        sel[-1 - sum(sel):] = True

    z = zFull[sel]
    rhoC = b['(.)rho_crit'][sel]
    rhoM = (b['(.)rho_b'] + b['(.)rho_cdm'])[sel]

    if '(.)rho_fld' in b.keys():
        rhoDE = b['(.)rho_fld'][sel]
        w = b['(.)w_fld'][sel]
    else:
        rhoDE = b['(.)rho_smg'][sel]
        w = b['w_smg'][sel]

    #####################
    DA = b['ang.diam.dist.'][sel]
    H = b['H [1/Mpc]'][sel]

    # Compute the exact growth rate
    #####################
    time_boundaries = [z[0], z[-1]]
    #
    OmegaDEwF_exact = interp1d(z, rhoDE/rhoC * w)
    OmegaMF = interp1d(z, rhoM/rhoC)
    # Use LSODA integrator as some solutions were wrong with RK45 and OK
    # with this.

    t_eval = z

    if z_output is not None:
        z_output = np.asarray(z_output)
        z_output[::-1].sort()  # Order it in a decreasing way
        H = interp1d(z, H)(z_output)
        DA = interp1d(z, DA)(z_output)
        t_eval = z_output

    if not fclass:
        if f0 is None:
            f0 = cosmo_extra.growthrate_at_z(cosmo, z[0])
        f = integrate.solve_ivp(cosmo_extra.fprime(OmegaDEwF_exact, OmegaMF), time_boundaries, [f0],
                                method='LSODA', dense_output=False, t_eval=t_eval).y[0]
    else:
        f = np.array([cosmo_extra.growthrate_at_z(cosmo, t) for t in t_eval])

    return np.array([t_eval, H, DA, f]), np.array([z_rec, DA_rec])

def angular_distance(Hinv_interp, z_output):
    return np.array([1./(1+z) * integrate.romberg(Hinv_interp, 0, z, divmax=20, vec_func=True) for z in z_output])

def remove_nans(arrays):
    output = []
    for array in arrays:
        output.append(array[~np.isnan(array)])

    return output

def compute_F(z, w, integ='trapz'):
    """
    F = int_0^lna' dlna w
    """
    lna = -np.log(1+z)
    if integ == 'trapz':  # Agreement with Romberg ~ 10^-5 (most of times)
        Fint = -np.array([integrate.trapz(w[lna >= lnai], lna[lna >= lnai]) for lnai in lna])
    elif integ == 'romberg':
        wintp = interp1d(lna, w)
        Fint = np.array([integrate.romberg(wintp, 0, lnai, divmax=20, vec_func=True) for lnai in lna])

    return Fint
