#!/usr/bin/python
from scipy import stats as stats
import numpy as np
import os

data = stats.norm.rvs(loc=0.3, scale=0.001, size=(10, 30000))
correlated_data = np.random.randint(low=-100, high=100, size=(len(data), len(data))).dot(data)
directory = 'mock_data-fit-logX-{}-Taylor-ln(1+z)/mock_data/'.format(len(data))
ofile = os.path.join(directory, 'mock_data-fit-logX-joined.txt')

header = ['mock data', ' '.join(['c_{}'.format(i) for i in range(len(data))])]

if not os.path.exists(directory):
    os.makedirs(directory)

with open(ofile, 'w') as f:
    for h in header:
        f.write('# ' + h +'\n')

with open(ofile, 'a') as f:
    np.savetxt(f, correlated_data.T)
