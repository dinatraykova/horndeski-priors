#!/bin/bash

output=$1
var=$2

case $output in
    *"-$var-"*) ;;
    *)      echo "Var is not present in output files. Aborting"
            exit
            ;;
esac

cd $output

for dir in *; do
    for i in {fit-$var,fit-$var-no_obs,reldev-obs,reldev-obs-no_obs,w0wa,w0wa-no_obs,w0wa_reldev-obs,w0wa_reldev-obs-no_obs,params,shooting}; do         
        cat ./${dir}/*[0-9]-${i}-[0-9]* > ${dir}/${dir}-${i}-joined.txt;
    done;
    cat ./${dir}/*.err > ${dir}/${dir}-joined.err;     
    rm ./${dir}/*0.txt ./${dir}/*[0-9].err
done

mkdir ./log

for i in *; do 
    if [ -d $i ] && [ $i != log ]; then
        echo $i; 
        grep \{\'parameters_smg\' $i/*joined.err | wc -l > log/${i}-all-errors.txt;
        grep 'Optimal parameters' $i/*joined.err | wc -l > log/${i}-fit-errors.txt;
    fi
done
