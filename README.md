RUFIAN is a code that RUn simulations, FIt parametrizations and ANalyze them.

Super-preliminary, undocumented

Dependencies:
  - work-in-class
  - numpy
  - scipy
  - matplotlib
  - mpi4py


Quick note about files:
  - `computation.py`: Define here your model and parameter priors
  - `compute_analysis_results.py`: Analyze the results (get observables, etc.)
  - `compute_analysis_analytical.py`: Analyze the sample from analytical priors (get observables, etc.)
  - `sample_analytical.py`: Define analytical distributions and sample in them
  - `binning.py`: Here the fitting is implemented
  - `analyze_coeffs.py`: Here the methods to analyze fit are implemented
  - `common.py`: Here there are some functions common among files.

The plotting utilities might not work as expected.

Credits:
  * Raúl Rodríguez Segundo (IFF - CSIC): Code name (RUFIAN)
