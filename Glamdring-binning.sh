#!/bin/bash
##############################################################################
##############################################################################
##############################################################################

model_i=$1
var=w                     # one of [F, w, logRho, logX, X]
fitting=Taylor_legacy     # one of [Taylor, Taylor_c0, Taylor_legacy_c0, Taylor_c1, Taylor_legacy_c1, bins, Pade]
xvar=\(1-a\)              # one of [ln\(1+z\), a, \(1-a\)]
z_low_redshift_fit=$2
w0wa_zmax=$2
npoints=$3
sigma=1e-3
sigma_rec=1e-4
ncoeffs=2
N_models=1000  # total number of models computed will be processes x N_models
plot=false
##############################################################################

### 1 -> monomial
### 1log -> monomial. phi_ini in [0.1, 70] logarithmically sampled
### 1i -> monomial. exponent in integers
### 1iTwice -> monomial. exponent in integers. Priors ~ x2
### 1iHalf -> monomial. exponent in integers. Priors ~x 1/2
### 1iInverse -> monomial. Negative exponent in integers
### 1o -> monomial. exponent in odds
### 1e -> monomial. exponent in evens
### 1Velocity -> monomial. exp in ints. +-log(phi_prime_ini). ini=BBN
### 1NegVelocity -> monomial. Negative exp in ints. +-log(phi_prime_ini). ini=BBN
### 2 -> modulus
### 2Twice -> modulus. Priors ~ x2
### 2Half -> modulus. Priors ~ x 1/2
### 3 -> axion
### 3Twice -> axion. Priors ~ x2
### 3Half -> axion. Priors ~ x 1/2
### 4 -> eft
### 4phi -> eft. Log(phi_ini) \in [0,1] no Lambda
### 4phiVelocity -> eft. Log(phi_ini) \in [0,1] no Lambda and +-log(phi_prime_ini). ini=BBN
### 4Lphi -> eft. Log(phi_ini) \in [-3,1] Lambda
### 4LphiTwice -> eft. Log(phi_ini) \in [-3,1] Lambda. Priors ~ x2
### 4LphiHalf -> eft. Log(phi_ini) \in [-3,1] Lambda. Priors ~ x 1/2
### 5 -> tracker

##############################################################################
##############################################################################

echo $model_i

if [ "$model_i" == "1" ]; then
    model_name=quintessence_monomial-reals
elif [ "$model_i" == "1log" ]; then
    model_name=quintessence_monomial-log-phii
elif [ "$model_i" == "1i" ]; then
    model_name=quintessence_monomial-ints
elif [ "$model_i" == "1iTwice" ]; then
    model_name=quintessence_monomial-ints-Twice
elif [ "$model_i" == "1iHalf" ]; then
    model_name=quintessence_monomial-ints-Half
elif [ "$model_i" == "1iInverse" ]; then
    model_name=quintessence_monomial-Inverse-ints
elif [ "$model_i" == "1o" ]; then
    model_name=quintessence_monomial-ints-odd
elif [ "$model_i" == "1e" ]; then
    model_name=quintessence_monomial-ints-even
elif [ "$model_i" == "1Velocity" ]; then
    model_name=quintessence_monomial-ints-Velocity-BBN
elif [ "$model_i" == "1NegVelocity" ]; then
    model_name=quintessence_monomial-Inverse-ints-Velocity-BBN
elif [ "$model_i" == "2" ]; then
    model_name=quintessence_modulus
elif [ "$model_i" == "2Twice" ]; then
    model_name=quintessence_modulus-Twice
elif [ "$model_i" == "2Half" ]; then
    model_name=quintessence_modulus-Half
elif [ "$model_i" == "3" ]; then
    model_name=quintessence_axion
elif [ "$model_i" == "3Twice" ]; then
    model_name=quintessence_axion-Twice
elif [ "$model_i" == "3Half" ]; then
    model_name=quintessence_axion-Half
elif [ "$model_i" == "4" ]; then
    model_name=quintessence_eft-lambda1-largeEF
elif [ "$model_i" == "4phi" ]; then
    model_name=quintessence_eft-logPhi
elif [ "$model_i" == "4phiTwice" ]; then
    model_name=quintessence_eft-logPhi-Twice
elif [ "$model_i" == "4phiHalf" ]; then
    model_name=quintessence_eft-logPhi-Half
elif [ "$model_i" == "4phiVelocity" ]; then
    model_name=quintessence_eft-logPhi-Velocity-BBN
elif [ "$model_i" == "4Lphi" ]; then
    model_name=quintessence_eft-lambda1-logPhi
elif [ "$model_i" == "5" ]; then
    model_name=quintessence_tracker
elif [ -f "$model_i" ]; then
    model_name=MontePython_chains
else
    echo "Error: Model number ($model_i) not recognized"
    exit 1
fi

##############################################################################
##############################################################################
##############################################################################


export PATH="/home/iff/cggarcia/codes/anaconda2/bin:$PATH"

HOMEDIR=/mnt/zfsusers/gravityls_3
PROJECTDIR=$HOMEDIR/hordenski_priors/binning
CODEDIR=$PROJECTDIR/codes
model_root=${model_name}-fit-${var}-${ncoeffs}-${fitting}-${xvar}
full_folder_model_name=${model_root}-$SLURM_JOBID
output=$PROJECTDIR/output/$full_folder_model_name

mkdir -p $output

#mpiexec.hydra -bootstrap slurm  $Python $SCRATCHDIR/computation.py ${model_i} $output ${N_models} $ncoeffs $var $fitting $xvar
python $CODEDIR/computation.py ${model_i} $output ${N_models} $ncoeffs $var $fitting $xvar $z_low_redshift_fit $sigma $sigma_rec $w0wa_zmax $npoints


$CODEDIR/join-files.sh $output $var

# ##############################################################################
# # Now analyze the data
# ##############################################################################
# if [ "$plot" = true  ]; then
# export I_MPI_FABRICS=shm:tcp
# 
# mpiexec.hydra -bootstrap slurm $Python -m mpi4py.futures $SCRATCHDIR/compute_analysis_results.py $HOMEDIR/hordenski_priors/binning/output/$full_folder_model_name

exit
