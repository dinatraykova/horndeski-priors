#!/usr/bin/python
import numpy as np
import sys
sys.path.append('/mnt/zfsusers/gravityls_3/hordenski_priors/binning/codes')  # Needed in Glamdring
from analyze_coeffs import Analyze
import common as co


if __name__ == "__main__":

    analyze = Analyze(
        preroot = sys.argv[1],
        fit_function=None,
        time_variable=None,
        no_obs=False,  # Analyze coeffs from fit w/o obs
        header_row=1,  # The row with the column names
        histlog=False, # Take log(coefficients)
        histlogcomplex=False, # Take log_modulus (raw coefficients)
        nbins=100,   # Bins for raw histograms
        basis_cut=0, #1e-2,  # Keep minimum dimensions that yield rel. err. in observables < basis_cut
        basis_cut_DArec=3e-3,
        points_in_H_DA_f_plot=20000, # If larger than the number of computed models,
                                    # it will just plot as much as computed models are.
        z_low_redshift_fit=None,    # None to use z_max_pk
        redshift_to_compare=np.array([0.1, 0.5, 1, 2, 3, 5, 10, 15]),
        remove_outliers_sigma=0,  # NOT CHECKED SINCE A LONG TIME AGO
        histlogauto=True,  # Prevent plot_lib using histlog if detected bin with P(bin) > 0.8
        remove_out_of_bounds=False,  # NOT CHECKED SINCE A LONG TIME AGO
        log_hist_threshold=0.5, # If histlogauto=True, change scale for hists with
                               # P(bin) > log_hist_threshold
        store_intermediate_computations=False,
        save_final_computations=True,
        multiprocess=True,
        acceptance=1e-3,  # Number of parameters allowed to be over the basis_cut
        chunksize=500,
        remove_crazy_functions=False,  # If True, need set_select_crazy_functions
                                        # NOT CHECKED SINCE A LONG TIME AGO
        analytical=True,
        fclass=True,
        use_class=True,
        class_params_parametrization=co.class_params_CPL_fld
    )

    analyze.prepare_histograms(
        run_residuals=True,
        only_residuals=False,
        run_rebinned_histograms=True,
        run_check_dim_reduction_sampling=False,
        hack_mono=True
    )

    analyze.compute_H_DA_f_redshift_to_compare()
    analyze.compute_H_DA_f_redshift_to_compare_sampled()
