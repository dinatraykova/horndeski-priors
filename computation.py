#!/usr/bin/python

from binning import Binning
from mpi4py import MPI
from glob import glob
import common as co
import numpy as np
import read_MP_params as rMP
import sys
import os
import work_in_class.wicmath as wicmath
import warnings



rank = '-' + str(MPI.COMM_WORLD.Get_rank())  # Comment if you don't use mpirun

zbins = np.arange(0, 3.001, 0.01)
abins = np.arange(0.01, 1.001, 5e-3)
# zbins = np.arange(0, 3.001, 1)
# abins = np.array([])

numbered = False  # Store which step is saved (i.e. doesn't fail)

params = {
    'Omega_Lambda': 0,
    'Omega_fld': 0,
    'Omega_smg': -1}


def common_params():
    params.update({'h': np.random.uniform(0.6, 0.8),
                   'Omega_cdm': np.random.uniform(0.15, 0.35)
                  })

if os.path.exists(sys.argv[1]):
    numbered = True
    bnfile = os.path.basename(sys.argv[1])
    if '__' in bnfile:
        root = os.path.dirname(sys.argv[1])
        try:
            chains = np.loadtxt(sys.argv[1])
        except ValueError:
            chains = rMP.read_unequal_chains(sys.argv[1])
        # suffix = '__' + sys.argv[1].split('__')[-1].strip('.txt')
        suffix = '_' + bnfile.strip('.txt')
    else:
        root = sys.argv[1]
        chains = rMP.read_chains(root)
        suffix = ''

    data = rMP.Data(root)
    gravity_model = data.cosmo_arguments['gravity_model']
    if gravity_model != "quintessence_monomial":
        chains = rMP.hack_chains_params_2_smg(chains, len(data.varied_params))
    modelPrefix = gravity_model + '-MontePythonChains' + suffix
    sys.argv[3] = chains.shape[0]

    def get_cosmo_params_it():
        for chainstep in chains:
            # for i in range(int(chainstep[0])):
            params = data.get_current_value_params_cosmo_dic(chainstep)
            if 'output' in params.keys():
                del(params['output'])
            if 'z_pk' in params.keys():
                del(params['z_pk'])
            if 'lensing' in params.keys():
                del(params['lensing'])
            if 'l_max_scalars' in params.keys():
                del(params['l_max_scalars'])
            yield params

    it = get_cosmo_params_it()
    params_func = it.next


elif sys.argv[1] == '1':  # Quintessence monomial. Exponential in reals
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-reals'

    def params_func():
        parameters_smg = [
            np.random.uniform(1, 7),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            np.random.uniform(1, 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

    #binning.compute_bins('./quintessence_monomial-w0_wa-201702071315.dat')

elif sys.argv[1] == '1log':  # Quintessence monomial. phi_ini \in [0.1, 70] in log scale
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-log-phii'

    def params_func():
        parameters_smg = [
            np.random.uniform(1, 7),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            10**np.random.uniform(-1, np.log10(70))  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '1i':  # Quintessence monomial. Exponential in integers
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-ints'

    def params_func():
        parameters_smg = [
            np.random.randint(1, 8),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            np.random.uniform(1, 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '1iTwice':  # Quintessence monomial. Exponential in integers
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-ints-Twice'

    def params_func():
        parameters_smg = [
            np.random.randint(1, 2 * 8),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            np.random.uniform(1, 2 * 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '1iHalf':  # Quintessence monomial. Exponential in integers
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-ints-Half'

    def params_func():
        parameters_smg = [
            np.random.randint(1, 0.5 * 8),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            np.random.uniform(1, 0.5 * 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '1iInverse':  # Quintessence monomial. Exponential in integers
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-Inverse-ints'

    def params_func():
        parameters_smg = [
            -np.random.randint(1, 8),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            np.random.uniform(1, 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '1o':  # Quintessence monomial. Exponential in odds
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-ints-odd'

    def params_func():
        parameters_smg = [
            #np.random.uniform(1, 7),  # "N"
            np.random.randint(0, 3)*2 + 1,  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            np.random.uniform(1, 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '1e':  # Quintessence monomial. Exponential in evens
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-ints-even'

    def params_func():
        parameters_smg = [
            #np.random.uniform(1, 7),  # "N"
            np.random.randint(1, 4)*2,  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            1e-100,  # phi_prime_ini,
            np.random.uniform(1, 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '1Velocity':  # Quintessence monomial.
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-ints-Velocity-BBN'

    def params_func():
        parameters_smg = [
            np.random.randint(1, 8),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            (1 if np.random.random() < 0.5 else -1) * 10**np.random.uniform(0,10),  # phi_prime_ini,
            np.random.uniform(1, 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model,
                       "a_ini_over_a_today_default": 1e-10,
                       "tol_initial_Omega_r": 1e-2})
        common_params()
        return params

elif sys.argv[1] == '1NegVelocity':  # Quintessence monomial.
    gravity_model = 'quintessence_monomial'
    modelPrefix = gravity_model + '-Inverse-ints-Velocity-BBN'

    def params_func():
        parameters_smg = [
            -np.random.randint(1, 8),  # "N"
            1., #10**np.random.uniform(-1, 1),  # "V0" tunned
            (1 if np.random.random() < 0.5 else -1) * 10**np.random.uniform(0,10),  # phi_prime_ini,
            np.random.uniform(1, 7)  # "phi_ini"
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model,
                       "a_ini_over_a_today_default": 1e-10,
                       "tol_initial_Omega_r": 1e-2})
        common_params()
        return params

elif sys.argv[1] == '2':
    gravity_model = "quintessence_modulus"
    modelPrefix = gravity_model

    def params_func():
        parameters_smg = [
            1.e-100,  # phi_prime_ini
            np.random.uniform(low=-1, high=1),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1) # V0, tunned
            10 ** np.random.uniform(low=-3, high=-1),  # E_D
            np.random.randint(low=1, high=6),  # p_D, The highest number is 5.
            np.random.randint(low=10, high=21),  # n_max, The highest number is 20.
            np.random.uniform(low=0, high=1)  # alpha
        ]
        parameters_2_smg = list(np.random.standard_normal(1 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

    #binning.compute_bins('./quintessence_modulus-w0_wa-201702071819.dat')

elif sys.argv[1] == '2Twice':
    gravity_model = "quintessence_modulus"
    modelPrefix = gravity_model + '-Twice'

    def params_func():
        parameters_smg = [
            1.e-100,  # phi_prime_ini
            np.random.uniform(low=-1 * 2, high=1 * 2),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1) # V0, tunned
            10 ** np.random.uniform(low=-3 - 1, high=-1 + 0.5),  # E_D < 1, since E_D^n must be decreasing
            np.random.randint(low=1, high=2 * 6),  # p_D, The highest number is 11.
            np.random.randint(low=10 - 5, high=21 + 5),  # n_max, The highest number is 25.
            np.random.uniform(low=0, high=2 * 1)  # alpha
        ]
        parameters_2_smg = list(np.random.standard_normal(1 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '2Half':
    gravity_model = "quintessence_modulus"
    modelPrefix = gravity_model + '-Half'

    def params_func():
        parameters_smg = [
            1.e-100,  # phi_prime_ini
            np.random.uniform(low=-1 * 0.5, high=1 * 0.5),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1) # V0, tunned
            10 ** np.random.uniform(low=-3 + 1, high=-1 - 0.5),  # E_D < 1, since E_D^n must be decreasing
            np.random.randint(low=1, high=0.5 * 6),  # p_D, The highest number is 11.
            np.random.randint(low=10, high=21 - 5),  # n_max, The highest number is 15.
            np.random.uniform(low=0, high=0.5 * 1)  # alpha
        ]
        parameters_2_smg = list(np.random.standard_normal(1 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '3':
    gravity_model = "quintessence_axion"
    modelPrefix = gravity_model

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3, high=-1)  # E_F

        parameters_smg = [
            1e-100,  # phi_prime_ini
            np.random.uniform(low=-np.pi/E_F, high=np.pi/E_F),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,  # E_F
            10 ** np.random.uniform(low=-3, high=-1),  # E_NP
            np.random.randint(low=10, high=21)  # n_max, The highest number is 20.
        ]

        parameters_2_smg = list(np.random.standard_normal(parameters_smg[-1] - 1))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

    #binning.compute_bins('./quintessence_axion-w0_wa-201702071506.dat')

elif sys.argv[1] == '3Twice':
    gravity_model = "quintessence_axion"
    modelPrefix = gravity_model + '-Twice'

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3 - 1, high=-1 + 0.5)  # E_F

        parameters_smg = [
            1e-100,  # phi_prime_ini
            np.random.uniform(low=-np.pi/E_F, high=np.pi/E_F),  # phi_ini --> without modification since function is cos()
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,  # E_F
            10 ** np.random.uniform(low=-3 - 1, high=-1 + 0.5),  # E_NP
            np.random.randint(low=10 - 5, high=21 + 5)  # n_max, The highest number is 25.
        ]

        parameters_2_smg = list(np.random.standard_normal(parameters_smg[-1] - 1))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '3Half':
    gravity_model = "quintessence_axion"
    modelPrefix = gravity_model + '-Half'

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3 + 1, high=-1 - 0.5)  # E_F

        parameters_smg = [
            1e-100,  # phi_prime_ini
            np.random.uniform(low=-np.pi/E_F, high=np.pi/E_F),  # phi_ini --> without modification since function is cos()
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,  # E_F
            10 ** np.random.uniform(low=-3 + 1, high=-1 - 0.5),  # E_NP
            np.random.randint(low=10, high=21 - 5)  # n_max, The highest number is 15.
        ]

        parameters_2_smg = list(np.random.standard_normal(parameters_smg[-1] - 1))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '4':
    gravity_model = 'quintessence_eft'
    modelPrefix = gravity_model + '-lambda1-largeEF'

    def params_func():
        #E_F = 10 ** np.random.uniform(low=-3, high=-1)
        E_F = 10 ** np.random.uniform(low=-1, high=0)

        parameters_smg = [
            1e-100,  # phi_prime_ini
            np.random.uniform(low=-1/E_F, high=1/E_F),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,
            np.random.randint(low=5, high=11),  # n_min, The highest number is 10.
            np.random.randint(low=5, high=11),  # n_Q, The highest number is 10.
            1. #np.random.standard_normal()  # zeta_Lambda, CC term
        ]

        parameters_2_smg = list(np.random.standard_normal(2 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

    #binning.compute_bins('./quintessence_eft-w0_wa-201702081639.dat')

elif sys.argv[1] == '4phi':
    gravity_model = 'quintessence_eft'
    modelPrefix = gravity_model + '-logPhi'

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3, high=-1)

        parameters_smg = [
            1e-100,  # phi_prime_ini
            10**np.random.uniform(low=0, high=1),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,
            np.random.randint(low=5, high=11),  # n_min, The highest number is 10.
            np.random.randint(low=5, high=11),  # n_Q, The highest number is 10.
            0. #np.random.standard_normal()  # zeta_Lambda, CC term
        ]

        parameters_2_smg = list(np.random.standard_normal(2 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '4phiTwice':
    gravity_model = 'quintessence_eft'
    modelPrefix = gravity_model + '-logPhi-Twice'

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3 - 1, high=-1 + 0.5)

        parameters_smg = [
            1e-100,  # phi_prime_ini
            10**np.random.uniform(low=0, high=1 + 0.5),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,
            np.random.randint(low=5, high=11 + 6),  # n_min, The highest number is 16. (min must be 5 since EFT main function order is 4th
            np.random.randint(low=5, high=11 + 6),  # n_Q, The highest number is 16. (same)
            0. #np.random.standard_normal()  # zeta_Lambda, CC term
        ]

        parameters_2_smg = list(np.random.standard_normal(2 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '4phiHalf':
    gravity_model = 'quintessence_eft'
    modelPrefix = gravity_model + '-logPhi-Half'

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3 + 1, high=-1 - 0.5)

        parameters_smg = [
            1e-100,  # phi_prime_ini
            10**np.random.uniform(low=0, high=1 - 0.2),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,
            np.random.randint(low=5, high=11 - 2),  # n_min, The highest number is 8. (min must be 5 since EFT main function order is 4th
            np.random.randint(low=5, high=11 - 2),  # n_Q, The highest number is 8. (same)
            0. #np.random.standard_normal()  # zeta_Lambda, CC term
        ]

        parameters_2_smg = list(np.random.standard_normal(2 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '4phiVelocity':
    gravity_model = 'quintessence_eft'
    modelPrefix = gravity_model + '-logPhi-Velocity-BBN'

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3, high=-1)

        parameters_smg = [
            (1 if np.random.random() < 0.5 else -1) * 10**np.random.uniform(0,10),  # phi_prime_ini,
            10**np.random.uniform(low=0, high=1),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,
            np.random.randint(low=5, high=11),  # n_min, The highest number is 10.
            np.random.randint(low=5, high=11),  # n_Q, The highest number is 10.
            0. #np.random.standard_normal()  # zeta_Lambda, CC term
        ]

        parameters_2_smg = list(np.random.standard_normal(2 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model,
                       "a_ini_over_a_today_default": 1e-10,
                       "tol_initial_Omega_r": 1e-2})
        common_params()
        return params

elif sys.argv[1] == '4Lphi':
    gravity_model = 'quintessence_eft'
    modelPrefix = gravity_model + '-lambda1-logPhi'

    def params_func():
        E_F = 10 ** np.random.uniform(low=-3, high=-1)

        parameters_smg = [
            1e-100,  # phi_prime_ini
            10**np.random.uniform(low=-3, high=1),  # phi_ini
            1.,  # 10 ** np.random.uniform(low=-1, high=1),  # V0, tunned
            E_F,
            np.random.randint(low=5, high=11),  # n_min, The highest number is 10.
            np.random.randint(low=5, high=11),  # n_Q, The highest number is 10.
            1. #np.random.standard_normal()  # zeta_Lambda, CC term
        ]

        parameters_2_smg = list(np.random.standard_normal(2 + parameters_smg[-2]))

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "parameters_2_smg": str(parameters_2_smg).strip('[]'),
                       "gravity_model": gravity_model})
        common_params()
        return params

elif sys.argv[1] == '5':
    gravity_model = 'quintessence_tracker'
    modelPrefix = gravity_model

    def params_func():

        parameters_smg = [
            # 1e-100,  # phi_prime_ini
            # 10**np.random.uniform(low=-3, high=1),  # phi_ini
            1, # K_i
            1, # P_i
            1.,  # V0, tunned
            np.random.randint(low=1, high=21),  # n
            1,  # m
            np.random.randint(low=2, high=21)  # lambda
        ]

        params.update({"parameters_smg": str(parameters_smg).strip('[]'),
                       "gravity_model": gravity_model})

        common_params()
        return params

else:
    raise ValueError('Model not implemented: {}'.format(sys.argv[1]))

#binning.set_bins(zbins, abins)
#binning.compute_bins_from_params(params_func, int(sys.argv[3]))

# for reverse in [False]:  # [False, True]:
#     for i, tmpVar in [('a', 'a')]:   # [('a', 'a'), ('z', 'z'), ('z+1', 'zPlus1'), ('log(a)', 'logA'), ('log(z+1)', 'logzPlus1')]:
#         filename = modelPrefix + '-' + tmpVar
#         if reverse:
#             filename += '-Reversed'
#
#         folder = os.path.join(sys.argv[2], filename)
#         try:
#             os.makedirs(folder)
#         except Exception as e:
#             if 'File exists' in e:
#                 pass
#             else:
#                 raise e
#
#         filename += rank  # Suffix for files in folder
#         log = open(os.path.join(folder, filename + '.err'), 'a')
#         sys.stderr = log
#
#         binning = Binning(filename, folder)
#         binning.set_Pade(5, 5, i, xReverse=reverse, increase=True)
#         binning.compute_Pade_from_params(params_func, int(sys.argv[3]))

filename = modelPrefix
folder = os.path.join(sys.argv[2], filename)
try:
    os.makedirs(folder)
except Exception as e:
    if 'File exists' in e:
        pass
    else:
        raise e
filename += rank
log = open(os.path.join(folder, filename + '.err'), 'a')
sys.stderr = log

binning = Binning(filename, folder)
#binning.set_bins(zbins, abins)
#binning.set_n_coeffs_fit(int(sys.argv[4]), fix_origin=True)
n_coeffs = int(sys.argv[4])

if sys.argv[6] == 'Taylor':
    fit_function = wicmath.Taylor
elif sys.argv[6] == 'Taylor_log':
    fit_function = wicmath.Taylor_log
elif sys.argv[6] == 'Taylor_c0':
    fit_function = wicmath.Taylor_c0
elif sys.argv[6] == 'Taylor_legacy_c0':
    fit_function = wicmath.Taylor_legacy_c0
elif sys.argv[6] == 'Taylor_c1':
    fit_function = wicmath.Taylor_c1
elif sys.argv[6] == 'Taylor_legacy_c1':
    fit_function = wicmath.Taylor_legacy_c1
elif sys.argv[6] == 'Taylor_legacy':
    fit_function = wicmath.Taylor_legacy
elif sys.argv[6] == 'bins':
    fit_function = wicmath.bins_equally_spaced
elif sys.argv[6] == 'Pade':
    fit_function = wicmath.Pade
else:
    sys.exit('Unrecognized fitting function.')

if sys.argv[9] == 'inf':
    sigma = np.inf
else:
    sigma = float(sys.argv[9])

if sys.argv[10] == 'inf':
    sigma_rec = np.inf
else:
    sigma_rec = float(sys.argv[10])

if n_coeffs <= 2:
    class_params_parametrization = co.class_params_CPL_fld
else:
    class_params_parametrization = co.class_params_w_expansion_fld

binning.set_fit(fit_function, n_coeffs, sys.argv[5], sys.argv[6], xvar=sys.argv[7],
                z_low_redshift_fit=float(sys.argv[8]), sigma=sigma,
                sigma_rec=sigma_rec, w0wa_zmax=float(sys.argv[11]),
                points_to_fit=int(sys.argv[12]),
                class_params_parametrization=class_params_parametrization)
binning.compute_fit_from_params(params_func, int(sys.argv[3]), numbered=numbered)
