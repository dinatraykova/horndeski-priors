#!/usr/bin/python

import common as co
import numpy as np
import os
from classy import Class
from classy import CosmoSevereError
import sys
import scipy.integrate as integrate
import work_in_class.wicmath as wicm
from work_in_class.wicmath import fit_pade
from scipy.interpolate import interp1d
import work_in_class.cosmo_extra as cosmo_extra

class Binning():
    def __init__(self, fname, outdir='./'):
        self._cosmo = Class()
        self._fname = fname
        self._outdir = outdir
        self._set_default_values()

    def _set_full_filenames(self, filesuffixes):
        """
        Return a list with the fullnames of the others, increasing their number
        in case they exist

        Additionally, set the full file names, of self._fparamsname and
        self.fshootname.
        """
        fullfilenames = []
        for suffix in filesuffixes + ['params', 'shooting']:
            fullfilenames.append(os.path.join(self._outdir, self._fname + '-' + suffix))

        i = 0

        bools = [True] * len(fullfilenames)

        while 1:
            for n, f in enumerate(fullfilenames):
                bools[n] = os.path.exists(f + '-%s.txt' % i)

            if True not in bools:
                break

            i += 1

        self._fparamsname = fullfilenames[-2] + '-%s.txt' % i
        self._fshootname = fullfilenames[-1] + '-%s.txt' % i

        return [f + '-%s.txt' % i for f in fullfilenames[:-2]]

    def _set_default_values(self):
        """
        Set default values of parameters lists.
        """
        self.params_smg = []
        self.params_2_smg = []
        self.h = []
        self.Omega_cdm = []

        self.gravity_model = []

        self._params = {"Omega_Lambda": 0,
                        "Omega_fld": 0,
                        "Omega_smg": -1,
                        'output': 'mPk',   #
                        'z_max_pk': 1080}  # Added for relative errors in f.

        self._computed = False
        self._path = []
        self._binType = ''

    def set_Pade(self, n_num, m_den, xvar='a', xReverse=False, accuracy=1e-3, increase=False, maxfev=0):
        """
        Set what Pade polynomial orders, temporal variable and its ordering use.
        """
        self.reset()
        self._PadeOrder = [n_num, m_den]
        self._Pade_xvar = xvar
        self._Pade_xReverse = xReverse
        self._Pade_maxfev = maxfev
        self._Pade_increase = increase
        self._Pade_accuracy = accuracy
        self._binType = 'Pade'

    def set_fit(self, fit_function, n_coeffs, variable_to_fit,
                fit_function_label='', bounds=(-np.inf, np.inf), p0=[],
                xvar='ln(1+z)', points_to_fit=10, z_low_redshift_fit=10,
                sigma=1e-3, sigma_rec=1e-4, w0wa_zmax=2, debug_fit=False,
                fit_with_class=True, class_params_parametrization=None):
        """
        Set the fitting_function and the number of coefficients.

        variable_to_fit must be one of 'F'or 'w'.

        fit_function_label will be written in the header of fit files.
        """
        self.reset()
        self._fit_function = fit_function
        self._n_coeffs = n_coeffs
        self._list_variables_to_fit = ['F', 'w', 'logRho', 'logX', 'X']
        if variable_to_fit in self._list_variables_to_fit:
            self._variable_to_fit = variable_to_fit
        else:
            raise ValueError('variable_to_fit must be one of {}'.format(self._list_variables_to_fit))

        self._fit_function_label = fit_function_label
        self._binType = 'fit'
        self._fFitnames = self._set_full_filenames(['fit-' + variable_to_fit,
                                                    'reldev-obs',
                                                    'fit-' + variable_to_fit + '-no_obs',
                                                    'reldev-obs-no_obs',
                                                    'w0wa', 'w0wa_reldev-obs',
                                                    'w0wa-no_obs',
                                                    'w0wa_reldev-obs-no_obs'])

        self._params.update({'z_max_pk': z_low_redshift_fit})
        self._fit_bounds = bounds
        self._p0 = p0
        list_fit_xvar = ['ln(1+z)', 'lna', 'a', '(1-a)']
        if xvar in list_fit_xvar:
            self._fit_xvar = xvar
        else:
            raise ValueError('xvar must be one of {}'.format(list_fit_xvar))
        self.points_to_fit = points_to_fit
        self.z_low_redshift_fit = z_low_redshift_fit
        self.sigma = sigma
        self.sigma_rec = sigma_rec

        self._w0wa_zmax = w0wa_zmax
        self.debug_fit = debug_fit
        self.fit_with_class = fit_with_class
        self.class_params_parametrization = class_params_parametrization
        if fit_with_class and (class_params_parametrization is None):
            raise ValueError('If you want to fit with class, you need to provide a function for CLASS: f(coeffs, class_params)')

    def set_bins(self, zbins, abins):
        """
        Set what bins to use and reset to avoid confusions.
        """
        self.reset()
        self._zbins = zbins
        self._abins = abins
        self._binType = 'bins'
        self._fwzname, self._fwaname = self._set_full_filenames(['wz-bins', 'wa-bins'] )

    def _read_from_file(self, path):
        """
        Return params for class from files used in quintessence Marsh.
        """
        with open(path) as f:
            f.readline()
            header = f.readline().split()[3:]  # Remove '#', 'w0', and 'wa'

        columns = np.loadtxt(path, unpack=True)[2:]  # Remove columns w0, wa

        for index_h, head in enumerate(header):
            if head[-1] == 'h':
                break

        self.params_smg = zip(*columns[:index_h])
        self.params_2_smg = [list(row[~np.isnan(row)]) for row in
                             np.array(zip(*columns[index_h+2:]))]
        self.h = columns[index_h]
        self.Omega_cdm = columns[index_h+1]

        self.gravity_model = os.path.basename(path).split('-')[0]

    def _params_from_row(self, row):
        """
        Set parameters.
        """
        params = self._params
        params.update({
            'parameters_smg': str(self.params_smg[row]).strip('[()]'),
            'h': self.h[row],
            'Omega_cdm': self.Omega_cdm[row],
            'gravity_model': self.gravity_model
        })

        if len(self.params_2_smg):
            params.update({
                'parameters_2_smg': str(self.params_2_smg[row]).strip('[()]')
            })

        return params

    def compute_bins(self, params):
        """
        Compute the w_i bins for the model with params.
        """
        wzbins = np.empty(len(self._zbins))
        wabins = np.empty(len(self._abins))
        self._params = params
        self._cosmo.set(params)
        try:
            self._cosmo.compute()
            for n, z in enumerate(self._zbins):
                wzbins[n] = self._cosmo.w_smg(z)
            for n, a in enumerate(self._abins):
                wabins[n] = self._cosmo.w_smg(1./a-1.)
            shoot = self._cosmo.get_current_derived_parameters(['tuning_parameter'])['tuning_parameter']
        except Exception as e:
            self._cosmo.struct_cleanup()
            self._cosmo.empty()
            raise e

        self._cosmo.struct_cleanup()
        self._cosmo.empty()

        return wzbins, wabins, shoot

    def _compute_common_init(self, params):
        """
        Common first steps for compute methods
        """
        self._params.update(params)
        self._cosmo.set(self._params)

        try:
            self._cosmo.compute()
            b = self._cosmo.get_background()
            shoot = self._cosmo.get_current_derived_parameters(['tuning_parameter'])['tuning_parameter']
        except Exception as e:
            self._cosmo.struct_cleanup()
            self._cosmo.empty()
            raise e

        return b, shoot

    def _compute_fit(self, params):
        """
        Fits self._fit_function to X, Y, with self._n_coeffs.
        """
        b, shoot = self._compute_common_init(params)

        # CLASS variables that we'll need to compute the observables.

        obs, rec = co.compute_observables_th(self._cosmo, self.z_low_redshift_fit, fclass=True)
        z, H, DA, f = obs

        try:
            popt, obsFit, p0, obsFit0 = self._compute_fit_fitted_function(b, obs, rec)

            w0wa_selection = z <= self._w0wa_zmax
            w0wa, obs_w0wa, p0_w0wa, obs0_w0wa = self._compute_fit_w0wa(b, obs[:, w0wa_selection], rec)
        except Exception as e:
            print(str(e))
            self._cosmo.struct_cleanup()
            self._cosmo.empty()
            sys.stderr.write(str(e) + '\n')

            raise e


        self._cosmo.struct_cleanup()
        self._cosmo.empty()

        #print popt
        #print obsFit
        #print p0
        #print obsFit0
        #print w0wa
        #print obs_w0wa
        #print p0_w0wa
        #print obs0_w0wa
        #print shoot
        return [popt, obsFit, p0, obsFit0, w0wa, obs_w0wa, p0_w0wa, obs0_w0wa], shoot

    def _compute_fit_fitted_function(self, background, obs, rec): # H, DA, f):
        b = background

        zobs, H, DA, f = obs
        z_rec, DA_rec = rec

        z = b['z'][b['z'] <= z_rec + 200]
        rhoM = (b['(.)rho_b'] + b['(.)rho_cdm'])[b['z'] <= z_rec + 200]
        rhoR = (b['(.)rho_g'] + b['(.)rho_ur'])[b['z'] <= z_rec + 200]
        rhoDE_0 = b['(.)rho_smg'][-1]

        # Note that lna is log(1+z). I used this name because is more convenient
        X, lna = self._get_fit_xvar_and_log1Plusz(z)
        Xrec, lna_rec = self._get_fit_xvar_and_log1Plusz(z_rec)

        index_to_compare = self._get_index_to_compare(z)
        index_to_compare_obs = self._get_index_to_compare(zobs)

        z_to_compare, X_to_compare = z[index_to_compare], X[index_to_compare]

        # Prepare observables we are going to compare.
        X_input = np.concatenate([X_to_compare, X_to_compare, X_to_compare, [Xrec]])
        obs_to_compare = np.concatenate([H[index_to_compare_obs],
                                         DA[index_to_compare_obs],
                                         f[index_to_compare_obs], [DA_rec]])
        obs_to_compare[obs_to_compare == 0] = 1e-100

        sigma = np.ones(obs_to_compare.shape[0]) * self.sigma
        sigma[-1] = self.sigma_rec

        ##############################################################
        ##############################################################
        ##############################################################
        if self.fit_with_class:
            cosmo = Class()
        def observables_from_fit_coeffs(xdata, coeffs, to_compare=True):
            if self.fit_with_class:
                class_params = self.class_params_parametrization(coeffs, self._cosmo.pars)
                cosmo.set(class_params)

                if to_compare:
                    teval = z_to_compare
                else:
                    teval = zobs

                try:
                    cosmo.compute()
                    obs, rec = co.compute_observables_th(cosmo, self.z_low_redshift_fit, teval, fclass=True) # , f[0])
                except Exception as e:
                    print(str(e))
                    obs = np.ones((4, teval.size)) * 1e100
                    rec = np.ones(2) * 1e100
                cosmo.struct_cleanup()
                cosmo.empty()
            else:
                yfit1 = self._fit_function(X, coeffs)   # Compute the whole yfit1 to have precission on observables

                rhoDE_fit, w_fit = self._get_array_of_rhoDE_wfit(z, rhoDE_0, yfit1)

                if to_compare:
                    indexes = index_to_compare
                else:
                    indexes = z <= zobs[0]

                obs, rec = \
                    co.compute_observables_fit(z, rhoM, rhoR,
                                                           rhoDE_fit, w_fit, f[0],
                                                           indexes,
                                                           z_rec)
            _, H_fit, DA_fit, f_fit = obs
            _, DA_rec = rec
            # There are rhoDE == np.inf as some parameters make the parametrization go crazy.
            # We change np.inf to huge numbers.
            H_fit[~np.isfinite(H_fit)] = 1e30 * np.max(H_fit[np.isfinite(H_fit)])
            f_fit[np.isnan(f_fit)] = 1e4 # np.inf
            obsFit = np.concatenate([H_fit, DA_fit, f_fit, [DA_rec]])
            obsFit[obsFit == 0] = 1e-100

            # print H_fit
            # print DA_fit
            # print f_fit
            # print DA_rec

            # print('chi2', np.sum(((obs_to_compare-obsFit) / (obs_to_compare * sigma))**2.))
            return obsFit

        ##############################################################
        ##############################################################
        ##############################################################

        # Fit Y first, to obtain a first value for p0
        # Get array of function to fit
        ###############################
        Y = self._get_array_of_variable_to_fit(b, z_rec + 200)
        p0, _ =  wicm.fit(self._fit_function, X, Y, self._n_coeffs, bounds=self._fit_bounds, p0=self._p0, ftol=1e-3, gtol=1e-3, xtol=1e-3)

        if self.points_to_fit < self._n_coeffs:
            method='trf'
        else:
            method=None

        popt, _ = wicm.fit(observables_from_fit_coeffs, X_input,
                           obs_to_compare, self._n_coeffs,
                           bounds=self._fit_bounds, p0=p0,  # Needed, otherwise chi2 huuuuge
                           method=method,
                           sigma=obs_to_compare*sigma)

        # print('params')
        # print('w/ obs', popt)
        # print('w/o obs', p0)
        # print('obs - obsFit ', obs_to_compare - obsFit)
        # print('obs - obsFit (w/o)', obs_to_compare - obsFit)

        # Fit
        obsFit = observables_from_fit_coeffs(X_input, popt)
        chi2 = np.sum(((obs_to_compare-obsFit) / (obs_to_compare * sigma))**2.)
        # print ((obs_to_compare-obsFit) / (obs_to_compare * sigma))**2.
        # maxchi2 = np.argmax(((obs_to_compare-obsFit) / (obs_to_compare * sigma))**2.)
        # print obs_to_compare[maxchi2]
        # print obsFit[maxchi2]

        obsFit = observables_from_fit_coeffs(X_input, popt, to_compare=False)
        obs_rd = self._get_obs_reldevs_part_fit(zobs, obsFit[:-1], H, DA, f)
        DA_reldev_rec = np.abs(obsFit[-1]/DA_rec - 1)


        # Fit no obs
        obsFit0 = observables_from_fit_coeffs(X_input, p0)
        chi20 = np.sum(((obs_to_compare-obsFit0) / (obs_to_compare * sigma))**2.)
        print('chi2', chi2, 'chi2 (w/o)', chi20)
        obsFit0 = observables_from_fit_coeffs(X_input, p0, to_compare=False)
        obs_rd0 = self._get_obs_reldevs_part_fit(zobs, obsFit0[:-1], H, DA, f)
        DA_reldev_rec0 = np.abs(obsFit0[-1]/DA_rec - 1)

        if self.debug_fit and ( (obs_rd[1] > 1e-2) or (obs_rd[3] > 1e-2) or
                                (obs_rd[5] > 1e-2) or (DA_reldev_rec > 3e-3) ):
            # Plot also fit w/o obs to compare
            self._plot_fit_debug(np.log(zobs +1), obsFit[:-1],
                                 obsFit0[:-1], H, DA, f, lna, Y,
                                 self._fit_function(X, popt),
                                 self._fit_function(X, p0),
                                 DA_reldev_rec=[DA_reldev_rec, DA_reldev_rec0])

        return (popt, np.concatenate([obs_rd, [z_rec, DA_reldev_rec, chi2]]),
                p0, np.concatenate([obs_rd0, [z_rec, DA_reldev_rec0, chi20]]))

    def _compute_fit_w0wa(self, background, obs, rec):
        # TODO: Use the one in common.py? I think it'd be enough if it returned
        # the coeffs of the fit without the observables. And compute the
        # reldevs afterwards.

        zobs, H, DA, f = obs
        z_rec, DA_rec = rec

        b = background
        zlim = zobs[0] # self._w0wa_zmax
        zFull = b['z']
        z = zobs  # b['z'][zFull <= zlim]
        lna = np.log(z+1)
        rhoM = (b['(.)rho_b'] + b['(.)rho_cdm'])[zFull <= zlim]
        rhoR = (b['(.)rho_g'] + b['(.)rho_ur'])[zFull <= zlim]
        rhoDE_0 = b['(.)rho_smg'][-1]
        w = b['w_smg'][zFull <= zlim]

        index_to_compare = self._get_index_to_compare(z)
        obs_to_compare = np.concatenate([H[index_to_compare], DA[index_to_compare], f[index_to_compare]])
        obs_to_compare[obs_to_compare == 0] = 1e-100
        sigma = np.ones(obs_to_compare.shape[0]) * self.sigma

        # popt, p0 = co.compute_fit_w0wa(z, rhoM, rhoR, rhoDE_0, H, DA, f, w, index_to_compare, self.sigma, basis='lna')
        if self.fit_with_class:
            popt, p0, class_params = co.compute_fit_w0wa_class(z, H, DA, f, w, self._cosmo.pars, index_to_compare, self.sigma, basis='(1-a)')
            cosmo = Class()
            cosmo.set(class_params)
            cosmo.compute()
            obsFit, obsFit_rec = co.compute_observables_th(cosmo, z[0], z, fclass=True)
            cosmo.struct_cleanup()

            cosmo.set({'w0_fld': p0[0], 'wa_fld': p0[1]})
            cosmo.compute()
            obsFit0, obsFit0_rec = co.compute_observables_th(cosmo, z[0], z, fclass=True)
            cosmo.struct_cleanup()
            cosmo.empty()

            yfit1 = popt[0] + popt[1] * z/(1+z)  # == (1-a)
            yfit0 = p0[0] + p0[1] * z/(1+z)  # == (1-a)
        else:
            popt, p0 = co.compute_fit_w0wa(z, rhoM, rhoR, rhoDE_0, H, DA, f, w, index_to_compare, self.sigma, basis='(1-a)')
            ##########
            # Use rho's up to z_rec to compare DA_rec too
            rhoM = (b['(.)rho_b'] + b['(.)rho_cdm'])[zFull <= z_rec + 200]
            rhoR = (b['(.)rho_g'] + b['(.)rho_ur'])[zFull <= z_rec + 200]
            z = zFull[zFull <= z_rec + 200]
            #########
            # Fit obs
            # yfit1 = popt[0] + popt[1] * np.log(1+z)
            yfit1 = popt[0] + popt[1] * z/(1+z)  # == (1-a)
            rhoDE_fit, w_fit = self._get_array_of_rhoDE_wfit(z, rhoDE_0, yfit1, w0wa=True)

            obsFit, obsFit_rec = co.compute_observables_fit(z, rhoM, rhoR,
                                                            rhoDE_fit, w_fit, f[0],
                                                            index_to_compare=(z <= zlim),
                                                            z_rec=z_rec)
            # Fit no obs
            # yfit0 = p0[0] + p0[1] * np.log(1+z)
            yfit0 = p0[0] + p0[1] * z/(1+z)  # == (1-a)
            rhoDE_fit, w_fit = self._get_array_of_rhoDE_wfit(z, rhoDE_0, yfit0, w0wa=True)
            obsFit0, obsFit0_rec = co.compute_observables_fit(z, rhoM, rhoR,
                                                              rhoDE_fit, w_fit,
                                                              f[0],
                                                              index_to_compare=(z <= zlim),
                                                              z_rec=z_rec)

        ##############

        obsFit[obsFit == 0] = 1e-100  # Change 0s to 1e-100 as in obs_to_compare
        obs_rd = self._get_obs_reldevs_part_fit(zobs, np.concatenate(obsFit[1:]), H, DA, f)
        DA_reldev_rec = np.abs(obsFit_rec[1]/DA_rec - 1)
        chi2 = np.sum(((obs_to_compare-np.concatenate(obsFit[1:, index_to_compare])) / (obs_to_compare * sigma))**2.)

        obsFit0[obsFit0 == 0] = 1e-100  # Change 0s to 1e-100 as in obs_to_compare
        obs_rd0 = self._get_obs_reldevs_part_fit(zobs, np.concatenate(obsFit0[1:]), H, DA, f)
        DA_reldev_rec0 = np.abs(obsFit0_rec[1]/DA_rec - 1)
        chi20 = np.sum(((obs_to_compare-np.concatenate(obsFit0[1:, index_to_compare])) / (obs_to_compare * sigma))**2.)
        print('chi2', chi2, 'chi2 (w/o)', chi20)

        if self.debug_fit and ( (obs_rd[1] > 1e-2) or (obs_rd[3] > 1e-2) or
                               (obs_rd[5] > 1e-2) ):
            self._plot_fit_debug(np.log(obsFit[0] + 1), np.concatenate(obsFit[1:]),
                                 np.concatenate(obsFit0[1:]), H, DA, f, lna, w, yfit1[z<=zlim], yfit0[z<=zlim], w0wa=True)

        return (popt, np.concatenate([obs_rd, [z_rec, DA_reldev_rec, chi2]]),
                p0, np.concatenate([obs_rd0, [z_rec, DA_reldev_rec0, chi20]]))

    def _plot_fit_debug(self, lna_obs, obsFit, obsFit0, H, DA, f, lna, Y, yfit1, yfit0, w0wa=False, DA_reldev_rec=None):
        # Plot also fit w/o obs to compare
        H_fit = obsFit[:len(obsFit)/3]
        DA_fit = obsFit[len(obsFit)/3:len(obsFit)/3*2]
        f_fit = obsFit[len(obsFit)/3*2:]

        H_fit0 = obsFit0[:len(obsFit0)/3]
        DA_fit0 = obsFit0[len(obsFit0)/3:len(obsFit0)/3*2]
        f_fit0 = obsFit0[len(obsFit0)/3*2:]

        from matplotlib import pyplot as plt
        fig, ax = plt.subplots(5,1, sharex=False)
        ax[0].plot(lna_obs, 0 * H )
        ax[0].plot(lna_obs, 100 * (H_fit/H - 1))
        ax[0].plot(lna_obs, 100 * (H_fit0/H - 1), '-.')
        ax[0].set_ylabel('rel. dev. H [%]')

        ax[1].plot(lna_obs, 0*DA)
        ax[1].plot(lna_obs, 100*(DA_fit/DA -1))
        ax[1].plot(lna_obs, 100*(DA_fit0/DA -1), '-.')
        ax[1].set_ylabel('rel. dev. DA [%]')

        ax[2].plot(lna_obs, 0 * f)
        ax[2].plot(lna_obs, 100 * (f_fit/f -1))
        ax[2].plot(lna_obs, 100 * (f_fit0/f -1), '-.')
        ax[2].set_ylabel('rel. dev. f [%]')

        ax[3].plot(lna, Y, label='exact')
        ax[3].plot(lna, yfit1, label='fit-obs')
        ax[3].plot(lna, yfit0, '-.', label='fit-no_obs')

        ax[4].plot(lna, Y * 0. , label='exact')
        ax[4].plot(lna, 100*(yfit1/Y -1), label='fit-obs')
        ax[4].plot(lna, 100*(yfit0/Y -1), '-.', label='fit-no_obs')
        if not w0wa:
            ax[3].set_ylabel(self._variable_to_fit)
            ax[4].set_ylabel('rel. dev. ' + self._variable_to_fit + '[%]')
        else:
            ax[3].set_ylabel('w')
            ax[4].set_ylabel('rel. dev. w [%]')

        ax[3].set_xlabel('log(1+z)')
        ax[4].set_xlabel('log(1+z)')
        ax[4].legend(loc=0)

        if DA_reldev_rec is not None:
            fig.suptitle('rel.dev.(DA_rec_obs) = {} [%]; rel.dev.(DA_rec_no-obs) = {} [%]'.format(100 * DA_reldev_rec[0], 100 * DA_reldev_rec[1]))

        plt.show()
        plt.close()

    def _get_fit_xvar_and_log1Plusz(self, z):
        """
        Return the X array to fit and log(1+z)
        """
        if self._fit_xvar == 'ln(1+z)':
            X = np.log(z + 1)
            lna = X
        elif self._fit_xvar == 'lna':
            X = - np.log(z + 1)
            lna = - X
        elif self._fit_xvar == 'a':
            X = 1. / (1 + z)
            lna = np.log(z + 1)
        elif self._fit_xvar == '(1-a)':
            X = (z / (1 + z))
            lna = np.log(z + 1)

        return X, lna

    def _get_array_of_variable_to_fit(self, background, zlim):
        b = background
        z = b['z']
        w = b['w_smg']

        if self._variable_to_fit == 'logX':
            logX = np.log(b['(.)rho_smg']/b['(.)rho_smg'][-1])
            Y = logX

        elif self._variable_to_fit == 'F':
            Y = co.compute_F(z, w)

        elif self._variable_to_fit == 'w':
            Y = w

        elif self._variable_to_fit == 'logRho':
            Y = np.log(b['(.)rho_smg'])

        elif self._variable_to_fit == 'X':
            Y = b['(.)rho_smg']/b['(.)rho_smg'][-1]

        return Y[z <= zlim]

    def _get_array_of_rhoDE_wfit(self, z, rhoDE_0, yfit1, w0wa=False):
        lna = np.log(1+z)

        if w0wa or (self._variable_to_fit == 'w'):
            Fint = co.compute_F(z, yfit1)
            rhoDE_fit = rhoDE_0 * np.exp(-3 * Fint) * (1+z)**3   # CHANGE WITH CHANGE OF FITTED THING

            # TODO: needed?
            # Xw_fit, w_fit = X, yfit1
            # w_fit = interp1d(Xw_fit, w_fit, bounds_error=False, fill_value='extrapolate')(X)
            w_fit = yfit1

        elif self._variable_to_fit == 'logX':
            rhoDE_fit = rhoDE_0 * np.exp(yfit1)   ###### CHANGE WITH CHANGE OF FITTED THING

            ThreewPlus1 = np.gradient(yfit1, lna, edge_order=2)
            w_fit = ThreewPlus1 / 3. - 1  # The minus sign is taken into account by the CLASS ordering.
            # w_fit = interp1d(Xw_fit, w_fit, bounds_error=False, fill_value='extrapolate')(lna)

        elif self._variable_to_fit == 'F':
            rhoDE_fit = rhoDE_0*np.exp(-3 * yfit1) *(1+z)**3   ###### CHANGE WITH CHANGE OF FITTED THING

            w_fit = - np.gradient(yfit1, lna, edge_order=2)
            # w_fit = -interp1d(Xw_fit, w_fit, bounds_error=False, fill_value='extrapolate')(lna)

        elif self._variable_to_fit == 'logRho':
            rhoDE_fit = np.exp(yfit1)   ###### CHANGE WITH CHANGE OF FITTED THING

            ThreewPlus1 = np.gradient(yfit1 - yfit1[-1], lna, edge_order=2)
            w_fit = ThreewPlus1 / 3. - 1  # The minus sign is taken into account by the CLASS ordering.
            # w_fit = interp1d(Xw_fit, w_fit, bounds_error=False, fill_value='extrapolate')(lna)
        elif self._variable_to_fit == 'X':
            rhoDE_fit = rhoDE_0 * yfit1   ###### CHANGE WITH CHANGE OF FITTED THING

            ThreewPlus1 = np.gradient(np.log(yfit1), lna, edge_order=2)
            # diff = interp1d(Xw_fit, diff, bounds_error=False, fill_value='extrapolate')(lna)
            # ThreewPlus1  = diff / yfit1
            w_fit = ThreewPlus1 / 3. - 1  # The minus sign is taken into account by the CLASS ordering.

        return rhoDE_fit, w_fit

    def _get_index_to_compare(self, z):
        index_to_compare = (np.linspace(len(z) - sum(z <= self.z_low_redshift_fit),
                                        len(z)-1, self.points_to_fit) +
                            0.5).astype(int) # Round float numbers z_to_compare

        index_to_compare = np.unique(index_to_compare)

        return index_to_compare

    def _get_obs_reldevs_part_fit(self, z, obsFit, H, DA, f):
        H_fit = obsFit[:len(obsFit)/3]
        DA_fit = obsFit[len(obsFit)/3:len(obsFit)/3*2]
        f_fit = obsFit[len(obsFit)/3*2:]

        DA[DA == 0] = DA_fit[DA_fit == 0] = 1e-100

        H_reldev = np.abs(H_fit/H - 1)
        zmax_H = z[np.argmax(H_reldev)]
        DA_reldev = np.abs(DA_fit/DA - 1)
        zmax_DA = z[np.argmax(DA_reldev)]
        f_reldev = np.abs(f_fit/f - 1)
        zmax_f = z[np.argmax(f_reldev)]

        return zmax_H, np.max(H_reldev), zmax_DA, np.max(DA_reldev), zmax_f, np.max(f_reldev)

    def compute_Pade_coefficients(self, params):
        """
        Returns the Pade coefficients for w computed from params and the maximum
        and minimum residual in absolute value.
        """
        self._params = params
        self._cosmo.set(params)

        try:
            self._cosmo.compute()
            b = self._cosmo.get_background()
            shoot = self._cosmo.get_current_derived_parameters(['tuning_parameter'])['tuning_parameter']
        except Exception as e:
            self._cosmo.struct_cleanup()
            self._cosmo.empty()
            raise e

        self._cosmo.struct_cleanup()
        self._cosmo.empty()

        xDict = {'z': b['z'],
                 'z+1': b['z']+1,
                 'a': 1./(b['z']+1),
                 'log(a)': -np.log(b['z']+1),
                 'log(z+1)': np.log(b['z']+1)}

        X = xDict[self._Pade_xvar]
        w = b['w_smg']

        if self._Pade_xReverse:
            X = X[::-1]
            w = w[::-1]

        PadeOrder = np.array(self._PadeOrder)

        if not self._Pade_increase:
            reduceOrder = [[0, 0], [1, 0], [0, 1], [2, 0], [2, 1], [3, 1]]
            orderList = PadeOrder - reduceOrder

        else:
            orderList = [[1, 1], [2, 0], [3, 0], [2, 1], [2, 2], [3, 1], [4, 0],
                         [2, 3], [3, 2], [4, 1], [5, 0], [3, 3], [4, 2], [5, 1],
                         [3, 4], [4, 3], [5, 2], [3, 5], [4, 4], [5, 3], [4, 5],
                         [5, 4], [5, 5]]

        r = np.array([np.inf])
        for order in orderList:
            # Increase order of Pade up to [5/5].
            try:
                padeCoefficientsTMP, padeFitTMP = fit_pade(X, w, *order,
                                                           maxfev=self._Pade_maxfev)
                rTMP = np.abs(padeFitTMP/w - 1.)
                if self._Pade_increase and (np.max(rTMP) > self._Pade_accuracy):
                    if np.max(rTMP) < np.max(r):
                        padeCoefficients = padeCoefficientsTMP
                        r = rTMP
                    continue
                else:
                    padeCoefficients = padeCoefficientsTMP
                    r = rTMP
                    break
            except Exception as e:
                if (order == orderList[-1]) and (len(r) == 1):
                    raise e

                continue

        zeros = (PadeOrder - order)

        numCoefficients = np.append(padeCoefficients[:order[0] + 1], [0.]*zeros[0])
        denCoefficients = np.append(padeCoefficients[order[0] + 1:], [0.]*zeros[1])
        padeCoefficients = np.concatenate([numCoefficients, denCoefficients])

        return np.concatenate([padeCoefficients, [np.min(r), np.max(r)]]), shoot

    def compute_bins_from_params(self, params_func, number_of_rows):
        """
        Compute the w_i bins for the models given by the function
        params_func iterated #iterations.
        """
        self._create_output_files()

        wzbins = []
        wabins = []
        params = []
        shoot = []

        for row in range(number_of_rows):
            sys.stdout.write("{}/{}\n".format(row+1, number_of_rows))
            params_tmp = params_func().copy()

            try:
                wzbins_tmp, wabins_tmp, shoot_tmp = self.compute_bins(params_tmp)
                wzbins.append(wzbins_tmp)
                wabins.append(wabins_tmp)
                params.append(params_tmp)
                shoot.append(shoot_tmp)
                # Easily generalizable. It could be inputted a list with the
                # desired derived parameters and store the whole dictionary.
            except Exception as e:
                sys.stderr.write(str(self._params) + '\n')
                sys.stderr.write(str(e))
                sys.stderr.write('\n')
                continue

            if len(wzbins) == 5:
                self._save_computed(params, shoot, [wzbins, wabins])

                params = []
                wzbins = []
                wabins = []
                shoot = []

        self._save_computed(params, shoot, [wzbins, wabins])

    def compute_Pade_from_params(self, params_func, number_of_rows):
        """
        Compute the w_i bins for the models given by the function
        params_func iterated #iterations.
        """
        self._create_output_files()

        wbins = []
        params = []
        shoot = []

        for row in range(number_of_rows):
            sys.stdout.write("{}/{}\n".format(row+1, number_of_rows))
            params_tmp = params_func().copy()

            try:
                wbins_tmp, shoot_tmp = self.compute_Pade_coefficients(params_tmp)
                wbins.append(wbins_tmp)
                params.append(params_tmp)
                shoot.append(shoot_tmp)
                # Easily generalizable. It could be inputted a list with the
                # desired derived parameters and store the whole dictionary.
            except Exception as e:
                sys.stderr.write(str(self._params) + '\n')
                sys.stderr.write(str(e))
                sys.stderr.write('\n')
                continue

            if len(wbins) == 5:
                self._save_computed(params, shoot, wbins)

                params = []
                wbins = []
                shoot = []

        self._save_computed(params, shoot, wbins)

    def compute_fit_from_params(self, params_func, number_of_rows, numbered=False):
        """
        Compute the fit for the models given by the function
        params_func iterated #iterations.

        The variable to fit is chosen in self.set_fit
        """
        self._create_output_files()


        coeffs = []
        params = []
        shoot = []
        row = 0

        number = 0
        number_list = []

        while row < number_of_rows:
            sys.stdout.write("{}/{}\n".format(row+1, number_of_rows))
            # params_tmp = params_func().copy()
            number += 1

            try:
                #coeffs_tmp, shoot_tmp = fit_variable_function(params_func())
                coeffs_tmp, shoot_tmp = self._compute_fit(params_func())
                coeffs.append(coeffs_tmp)
                params.append(self._params.copy())
                shoot.append(shoot_tmp)
                # Easily generalizable. It could be inputted a list with the
                # desired derived parameters and store the whole dictionary.
            except StopIteration:
                sys.stdout.write('Stop iteration found in params_func\n')
                break
            except Exception as e:
                sys.stderr.write(str(self._params) + '\n')
                sys.stderr.write(str(e))
                sys.stderr.write('\n')
                continue

            if len(coeffs) == 5:
                self._save_computed(params, shoot, coeffs)

                params = []
                coeffs = []
                shoot = []

            row += 1
            number_list.append(number)

        if len(coeffs):
            self._save_computed(params, shoot, coeffs)

        if numbered:
            np.savetxt(os.path.join(self._outdir, self._fname + '-' + 'numbers.txt'), number_list,
                       fmt="%d", header='Model index (starting from 1) on chain')

    def compute_bins_from_file(self, path):
        """
        Compute the w_i bins for the models given in path.
        """
        if self._computed is True:
            print("Bins already computed. Use reset if you want to compute it again")
            return

        self._path = path

        self._read_from_file(path)

        def params_gen(length):
            row = 0
            while row < length:
                yield self._params_from_row(row)
                row += 1

        params = params_gen(len(self.params_smg))

        self.compute_bins_from_params(params.next,
                                      len(self.params_smg))

    def _create_output_files(self):
        """
        Initialize the output files.
        """
        # TODO: Add check if files exist
        with open(self._fparamsname, 'a') as f:
            f.write('# ' + "Dictionary of params to use with cosmo.set()" + '\n')

        with open(self._fshootname, 'a') as f:
            f.write('# ' + "Shooting variable value" + '\n')

        if self._binType == 'bins':
            with open(self._fwzname, 'a') as f:
                f.write('# ' + "Bins on redshift" + '\n')
                f.write('# ' + str(self._zbins).strip('[]').replace('\n', '') + '\n')

            with open(self._fwaname, 'a') as f:
                f.write('# ' + "Bins on scale factor" + '\n')
                f.write('# ' + str(self._abins).strip('[]').replace('\n', '') + '\n')
        elif self._binType == 'Pade':
            with open(self._fPadename, 'a') as f:
                f.write('# ' + "Pade fit for temporal variable {} \n".format(self._Pade_xvar))
                coeff_header_num = ['num_{}'.format(n) for n in range(self._PadeOrder[0] + 1)]
                coeff_header_den = ['den_{}'.format(n + 1) for n in range(self._PadeOrder[1])]
                res_header = ['min(residual)', 'max(residual)']
                f.write('# ' + ' '.join(coeff_header_num + coeff_header_den + res_header) + '\n')
        elif self._binType == 'fit':
            fit_header = '# ' + "{} fit for temporal variable {} of {}. ".format(self._fit_function_label, self._fit_xvar, self._variable_to_fit)
            coeffs_line = '# ' + ' '.join(['c_{}'.format(n) for n in range(self._n_coeffs)])

            res_header_ar = ['zmax_H', 'max(rel.dev. H)', 'zmax_DA', 'max(rel.dev. D_A)', 'zmax_f', 'max(rel.dev. f)',
                             'z_rec', 'rel.dev.DA', 'chi2(obs)']
            res_header_rec_ar = res_header_ar + ['z_rec', 'rel.dev.DA']

            res_header = '# ' + ' '.join(res_header_ar)
            # res_header = '# ' + ' '.join(res_header_ar + ['chi2(obs)'])
            # res_header_rec = '# ' + ' '.join(res_header_rec_ar + ['chi2(obs)'])

            fit_header_w0wa = '# ' + "w0wa (zmax = {}) fit through w ~ w0 + wa (1-a).".format(self._w0wa_zmax)

            for i, filename in enumerate(self._fFitnames):
                with open(filename, 'a') as f:
                    if i == 0:  # Fit with observables
                        string1 = fit_header + 'Chi2(H, DA, f), with {} points per obs upto z ~ {} + DA(z_rec).'.format(self.points_to_fit, self.z_low_redshift_fit)
                        string2 = coeffs_line

                    elif i == 1: # reldev observables
                        string1 = fit_header + 'Chi2(H, DA, f), with {0} points per obs upto z ~ {1} + DA(z_rec). max reldev with z<={1}. sigma(z<{1}) = {2}%, sigma(DArec) = {3}%'.format(self.points_to_fit, self.z_low_redshift_fit, 100. * self.sigma, 100. * self.sigma_rec)
                        string2 = res_header

                    elif i == 2: # Fit without observables
                        string1 = fit_header + 'Chi2({})'.format(self._variable_to_fit)
                        string2 = coeffs_line

                    elif i == 3: # reldev observables fit -noobs
                        string1 = fit_header + 'Chi2({}). max reldev with z<={}'.format(self._variable_to_fit, self.z_low_redshift_fit)
                        string2 = res_header

                    elif i == 4: # w0wa
                        string1 = fit_header_w0wa +  'Chi2(H, DA, f), with {} points per obs upto z ~ {}'.format(self.points_to_fit, self.z_low_redshift_fit)
                        string2 = '# w0 wa'

                    elif i == 5: # w0wa-reldev
                        string1 = fit_header_w0wa +  'Chi2(H, DA, f), with {} points per obs upto z ~ {} and sigma = {} %'.format(self.points_to_fit, self._w0wa_zmax, self.sigma * 100)
                        string2 = res_header

                    elif i == 6: # w0wa without observables
                        string1 = fit_header_w0wa + 'Chi2(w)'
                        string2 = '# w0 wa'

                    elif i == 7: # w0wa-reldev -noobs
                        string1 = fit_header_w0wa +  'Chi2(w). chi2(obs) for {} points per obs, upto z ~ {} and sigma = {} %'.format(self.points_to_fit, self._w0wa_zmax, self.sigma * 100.)
                        string2 = res_header

                    f.write(string1 + '\n')
                    f.write(string2 + '\n')

    def _save_computed(self, params, shoot, wbins):
        """
        Save stored iterations in file.
        """
        with open(self._fparamsname, 'a') as f:
            for i in params:
                f.write(str(i)+'\n')

        with open(self._fshootname, 'a') as f:
            np.savetxt(f, shoot)

        if self._binType == 'bins':
            wzbins, wabins = wbins
            with open(self._fwzname, 'a') as f:
                np.savetxt(f, wzbins)

            with open(self._fwaname, 'a') as f:
                np.savetxt(f, wabins)
        elif self._binType == 'Pade':
            with open(self._fPadename, 'a') as f:
                np.savetxt(f, wbins)
        elif self._binType == 'fit':
            for i, filename in enumerate(self._fFitnames):
                data = np.stack(np.asarray(wbins)[:, i], axis=0)
                with open(filename, 'a') as f:
                    np.savetxt(f, data)

    def reset(self):
        """
        Reset class
        """
        self._cosmo.struct_cleanup()
        self._cosmo.empty()
        self._set_default_values()
