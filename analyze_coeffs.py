#!/usr/bin/python2.7
from classy import Class
from histograms import Histograms
from mpi4py.futures import MPIPoolExecutor
from work_in_class import wicmath
from work_in_class import cosmo_extra
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
import common as co
import numpy as np
import os
import scipy.integrate as integrate
import scipy.stats as stats
import sys
import warnings
from glob import glob


##### TODO ######
#
#    Read coeffs
#    Read hi_class parameters to compute the exact observables (low redshift & DA(z_rec)
#    Diagonalize coeffs
#    Compute reldev wrt the hi_class obervables vs number of dimensions in diagonalized basis.
#    Compute reldev wrt the hi_class logX
#        Use log(1+z) from hi_class to compare the observables
#    Compute reldev wrt the fitted logX (that using al parameters)
#    Compute reldev wrt the observables obtained with fitted logX.
#    Check reliability of the distributions:
#        Sample in the diagonal distributions (histlogauto --> sample in log scale for certain cases)
#        Compute the observables for given redshifts and # dimensions.
#        Compute w0-wa for # dimensions
#
##### ODOT ######


# IMPORTANT NOTE: ALL ln(a) is actually ln(1+z) = -ln(a).
# IMPORTANT NOTE: Everything is set s.t. lna is actually -ln(a)
# IMPORTANT NOTE: This is this way because binning.py fits using log(1+z)
# REMINDER: DA and f are OK because they used z, not ln(a)

#########
# TODO: Consider moving these two functions to a different module or wic
#########
def _maxreldev_dimensions(exact_functions, array_reduced_functions):
    reldev = []
    for reduced_functions in array_reduced_functions:
        reldev.append(_maxreldev(exact_functions, reduced_functions))

    return np.array(reldev)

def _maxreldev(exact_functions, reduced_functions):

    exact_functions[exact_functions == 0] = 1e-100
    reduced_functions[reduced_functions == 0] = 1e-100

    # Use |x - y|/max(x,y) as F -> 0 at z = 0
    # Use np.nanmax as F = 0 if we choose c_0 = 0.
    # return np.nanmax(np.abs(reduced_functions - exact_functions) /
    #                  np.max([np.abs(exact_functions),
    #                          np.abs(reduced_functions)], axis=0), axis=1)

    return np.max(np.abs(reduced_functions / exact_functions - 1), axis=1)
#########

def _compute_H_DA_f_from_array_wrapper(arr):
    old_settings = np.seterr(over='raise')
    z = arr[0]
    rhoM, rhoR, rhoDE_fit, w_fit = arr[1]
    index_to_compare = arr[2]
    f0 = arr[3]
    z_rec = arr[4]

    try:
        if np.inf in rhoDE_fit:
            raise Exception('Inf in rhoDE\n')
        obs, rec = co.compute_observables_fit(z, rhoM, rhoR, rhoDE_fit, w_fit, f0, index_to_compare, z_rec)
    except Exception as e:
        np.seterr(**old_settings)
        sys.stderr.write('Error in child: ' + str(e) + '\n')
        nanArray = np.array([np.nan]*sum(index_to_compare))
        return (nanArray, nanArray, nanArray), (np.nan, np.nan)

    np.seterr(**old_settings)

    # Remove z from obs
    return obs[1:], rec

def _compute_w0wa_fit_from_array_wrapper(arr):
    old_settings = np.seterr(over='raise')

    z = arr[0]
    rhoM, rhoR, rhoDE_fit, H, DA, f, w = arr[1]

    try:
        if np.any(np.isinf(arr[1])) or np.any(np.isnan(arr[1])):
            raise Exception('Inf or nan in rhoDE or observables\n')
        popt, _ = co.compute_fit_w0wa(z, rhoM, rhoR, rhoDE_fit[-1], H, DA, f, w)

    except Exception as e:
        np.seterr(**old_settings)
        sys.stderr.write('Error in child: ' + str(e) + '\n')
        return np.nan, np.nan

    np.seterr(**old_settings)
    return popt

def _compute_H_DA_f_from_class_wrapper(arr):
    params = arr[0]
    z_low_redshift_fit = arr[1]
    z_output = arr[2]
    f0 = arr[3]
    fclass = arr[4]

    cosmo = Class()
    try:
        cosmo.set(params)
        cosmo.compute()
        obs, rec = co.compute_observables_th(cosmo, z_low_redshift_fit, z_output, f0=f0, fclass=fclass)
    except Exception as e:
        cosmo.struct_cleanup()
        cosmo.empty()
        sys.stderr.write('Error in child: ' + '\n')
        sys.stderr.write('parameters_smg = ' + str(params) + '\n')
        sys.stderr.write(str(e) + '\n')
        nanArray = np.array([np.nan]*z_output.shape[0])
        return (nanArray, nanArray, nanArray), (np.nan, np.nan), (np.nan, np.nan, np.nan)

    b = cosmo.get_background()
    rho0_m = b['(.)rho_b'][-1] + b['(.)rho_cdm'][-1]
    rho0_rad = b['(.)rho_g'][-1] + b['(.)rho_ur'][-1]
    if '(.)rho_smg' in b.keys():
        rho0_DE = b['(.)rho_smg'][-1]
    else:
        rho0_DE = b['(.)rho_fld'][-1]

    cosmo.struct_cleanup()
    cosmo.empty()

    # Remove z from obs
    return obs[1:], rec, (rho0_m, rho0_rad, rho0_DE)

def _compute_rhos_from_class(pars):
    cosmo = Class()
    cosmo.set(pars)
    cosmo.compute()
    b = cosmo.get_background()
    rho0_m = b['(.)rho_b'][-1] + b['(.)rho_cdm'][-1]
    rho0_rad = b['(.)rho_g'][-1] + b['(.)rho_ur'][-1]
    if '(.)rho_smg' in b.keys():
        rho0_DE = b['(.)rho_smg'][-1]
    else:
        rho0_DE = b['(.)rho_fld'][-1]
    cosmo.struct_cleanup()
    cosmo.empty()
    return rho0_m, rho0_rad, rho0_DE

def print_old_function_warning(function_name):
    warnings.warn('Using {}. It has not been tested in a long time. Check your results!!!!'.format(function_name))

class Analyze():
    def __init__(self, preroot, fit_function=None, time_variable=None,
                 no_obs=False, header_row=1, histlog=False,
                 histlogcomplex=False, nbins=100, basis_cut=1e-2,
                 basis_cut_DArec=3e-3, points_in_H_DA_f_plot=20000,
                 z_low_redshift_fit=None,
                 redshift_to_compare=np.array([0.1, 0.5, 1, 2, 5, 10]),
                 remove_outliers_sigma=0, histlogauto=True,
                 remove_out_of_bounds=False, log_hist_threshold=0.5,
                 store_intermediate_computations=False,
                 save_final_computations=True, multiprocess=True,
                 acceptance=1e-3, chunksize=100, remove_crazy_functions=False,
                 analytical=False, use_class=True, fclass=True,
                 class_params_parametrization=None):
        """ The constructor of the Analyze class

        preroot (str): path of the folder containing the binning.py results
        fit_function (str or None or function): If None, read the header of the
            fit file & set the fit_function automatically. If str, the name of the
            fit_function. If function, the fit_function that will be used.
        time_variable (None or str): time variable used to obtained the
        coefficients of the parametrization. If None, it will be read from fit
        file header.
        z_low_redshift_fit = None or int. If None it will use z_max_pk from param file
        redshift_to_compare: redshift_to_compare with sampled. It must be <
            z_low_redshift_fit
        fclass = Use class to compute f, not implemented eq. Default is True
        class_params_parametrization = None. Set to a function that outputs params for
                                        class for the parametrized theory.
        use_clas = True computes observables with Class
        """

        self.preroot = preroot
        self.modelname = self._find_modelname()
        self.root = os.path.join(preroot, self.modelname)

        self.model_title = self._find_model_title()

        self.filepaths = self._get_filepaths(no_obs)

        if save_final_computations and (not os.path.exists(self.filepaths['outdir'])):
            os.makedirs(self.filepaths['outdir'])

        fit_function_name, \
            self.fitted_function_filename, \
                self.time_variable = self._read_info_header_fit_file()

        if time_variable is not None:
            if time_variable != self.time_variable:
                warnings.warn('Input time_variable ({}) is not the same as the one in fit file ({}). Proceeding anyway.'.format(time_variable, self.time_variable))
            self.time_variable = time_variable


        self.fitted_function_label = self._get_fitted_function_label(self.fitted_function_filename)

        if fit_function is None:
            self.set_fit_function(fit_function_name)
        else:
            self.set_fit_function(fit_function)

        self.class_params = self._read_class_params()

        # Make sure logX, w, etc is computed before z_rec, s.t. we can choose
        # the lower redshits to compare and DA_rec.
        # f will be computed only in the desired ranged using CLASS to obtain f0
        # When checking the sampled cases, integrate from early times with f0 = 1
        ztc = np.array(redshift_to_compare)
        if self.class_params[0]['z_max_pk'] not in ztc:
            ztc = np.concatenate([ztc, [self.class_params[0]['z_max_pk']]])

        self.z_low_redshift_fit = self.class_params[0]['z_max_pk']
        if (z_low_redshift_fit is not None):
            if z_low_redshift_fit <= self.z_low_redshift_fit:
                self.z_low_redshift_fit = z_low_redshift_fit
            else:
                warnings.warn('z_low_redshift_fit > z_max_pk. Overriding z_max_pk = {} from param file\n'.format(self.z_low_redshift_fit))
                self.z_low_redshift_fit = z_low_redshift_fit
                for p in self.class_params:
                    p['z_max_pk'] = z_low_redshift_fit

        if np.any(ztc > self.z_low_redshift_fit):
            warnings.warn("redshift_to_compare must be <= z_low_redshift_fit (= {}). Removing higher z's\n".format(self.z_low_redshift_fit))
            ztc = ztc[ztc <= self.z_low_redshift_fit]
        if self.z_low_redshift_fit not in ztc:
            ztc = np.concatenate([ztc, [self.z_low_redshift_fit]])
        ztc[::-1].sort()
        self.redshift_to_compare = ztc

        # Use z > z_rec to be able to interp easily all models.
        lna = np.linspace(np.log(1200), 0, 1000)  # Note: No - sign. Just more convenient name.
        z = np.exp(lna) - 1
        z = np.unique(np.concatenate([z, ztc]))
        z[::-1].sort()
        self.z = z
        self.lna = np.log(z+1)

        self.time_array = self._get_time_array()
        self.time_label = 'log(1+z)'

        self.redshift_reldevs = (z <= self.z_low_redshift_fit)
        self.redshift_to_compare_indices = \
            np.array([np.where(np.isclose(z[self.redshift_reldevs], zi))[0][0]
                      for zi in ztc])

        self.header_row = header_row
        self.histlog = histlog
        self.histlogcomplex = histlogcomplex
        self.histtransform = False
        self.histlogauto = histlogauto
        self.log_hist_threshold = log_hist_threshold
        self.nbins = nbins
        self.basis_cut = basis_cut
        self.basis_cut_DArec = basis_cut_DArec
        self.selection = []
        self.points_in_H_DA_f_plot = points_in_H_DA_f_plot

        self.acceptance = acceptance

        if analytical:
            suffix_sampled = '-analytical'
        elif histlogauto:
            suffix_sampled = '-histlogauto_{}'.format(self.log_hist_threshold)
        elif histlog:
            suffix_sampled = '-histlog'
        else:
            suffix_sampled = '-linear'

        self.suffix_sampled = suffix_sampled

        # Filled in prepare_histograms() #
        self.hist_orig_scale_orig = Histograms()
        self.hist_corrected_scale_orig = Histograms()
        self.hist_diagonal_orig = Histograms()
        self.hist_corrected_scale_rb_orig = Histograms()
        self.hist_corrected_scale_orig_reduced = Histograms()
        self.hist_orig_scale_orig_reduced = Histograms()
        self.hist_diagonal_sampled = Histograms()
        self.hist_corrected_scale_sampled = Histograms()
        self.hist_orig_scale_sampled = Histograms()
        self.residualsHist = Histograms()
        self.hist_w0wa_fit = Histograms()
        self.hist_w0wa_fit_sampled = Histograms()

        self.hist_w0wa_fit.bins = ['w_0', 'w_a']
        self.hist_w0wa_fit_sampled.bins = self.hist_w0wa_fit.bins

        self._hist_diagonal_sampled_scale = []

        self.coeffs_reduced = []

        self.dim_red = 0  # Overwritten in prepare_histograms()

        self.analytical = analytical
        if analytical:
            self.class_params_analytical = self._read_class_params(analytical)

        #########################################################
        #     WARNING: OLD FUNCTIONS. UNEXPECTED BEHAVIOR!!     #
        #########################################################
        if remove_outliers_sigma:
            print_old_function_warning('remove_outliers_sigma')
        self.remove_outliers_sigma = remove_outliers_sigma
        self.percentage_outliers_removed = 0
        if remove_out_of_bounds:
            print_old_function_warning('remove_out_of_bounds')
        self.remove_out_of_bounds = remove_out_of_bounds
        self.percentage_ofb_removed = 0
        if remove_crazy_functions:
            print_old_function_warning('remove_crazy_functions')
        self.remove_crazy_functions = remove_crazy_functions
        self.percentage_remove_crazy_functions = 0
        #########################################################
        #########################################################

        self.chunksize = chunksize


        np.set_printoptions(precision=2)

        self.relative_errors_vs_dimmensions_fitted_function = []
        self.fitted_functions = []
        self.fitted_functions_reduced = []
        self.fitted_functions_sampled = []
        self.w = []
        self.w_sampled = []
        self.w_reduced = []

        # TODO: use dictionary instead of so many variables
        obs = {'H': [],
              'DA': [],
              'f': [],
              'DA_rec': [],
              'z_rec': []}

        self.obs_empty = obs.copy()
        self.obs = obs
        self.obs_reduced = obs.copy()
        self.obs_sampled = obs.copy()
        self.obs_reldev = obs.copy()
        self.obs_reldev['max'] = []  # Max of low-redshift obs_reldevs
        self.obs_to_compare = obs.copy()
        self.obs_to_compare_reduced = obs.copy()
        self.obs_sampled_to_compare = obs.copy()

        self.store_intermediate_computations = store_intermediate_computations
        self.save_final_computations = save_final_computations
        self.multiprocess = multiprocess
        self.use_class = use_class
        self.fclass = fclass
        self.class_params_parametrization = class_params_parametrization

        if use_class and (self.class_params_parametrization is None):
            raise ValueError('If you want to fit with class, you need to provide a function for CLASS when initializing Analyze: f(coeffs, class_params)')

    def _read_class_params(self, analytical=False):
        msg = r"In order to use original cosmological " + \
                        r"parameters to obtain rho_0 eval will be " + \
                        r"used. It is dangerous as someone could have " + \
                        r"modified the command to do bad things!!!\n"

        if not analytical:
            fpath = self.filepaths['params']
        else:
            fpath = self.filepaths['params_analytical']

        warnings.warn(msg)

        params_arr = []
        with open(fpath) as f:
            for params in f:
                if '#' in params:
                    continue
                # NOTE: USING EVAL IS DANGEROUS!
                d = eval(params)
                params_arr.append(d)

        return params_arr

    def _get_filepaths(self, no_obs):
        """
        Return a dictionary with filepaths
        """
        if no_obs:
            suffix = '-no_obs'
        else:
            suffix = ''

        prepath = os.path.join(self.root, self.modelname)

        filepaths = {}

        fitpaths = glob(prepath + '-fit-*-joined.txt')
        for i in fitpaths:
            if ("no_obs" not in i) and (not no_obs):
                fitpath = i
                break
            elif ("no_obs" in i) and (no_obs):
                fitpath = i
                break

        fitpath_reldev = prepath + '-reldev-obs{}-joined.txt'.format(suffix)
        paramspath = prepath + '-params-joined.txt'
        w0wapath = prepath + '-w0wa{}-joined'.format(suffix)
        w0wapath_reldev = prepath + '-w0wa_reldev-obs{}-joined'.format(suffix)
        outdir_data = os.path.join(self.root, 'computed_data{}'.format(suffix))
        paramspath_anal = prepath + '-params-analytical.txt'
        analpath = prepath + '-analytical-sampled.txt'

        filepaths = {'fit': fitpath,
                     'fit_reldev': fitpath_reldev,
                     'w0wa': w0wapath,
                     'w0wa_reldev': w0wapath_reldev,
                     'outdir': outdir_data,
                     'params': paramspath,
                     'params_analytical': paramspath_anal,
                     'analytical': analpath}

        sys.stdout.write(filepaths['fit'] + '\n')

        return filepaths

    def _read_info_header_fit_file(self):
        """
        Read the header of the fit file and return the name of the fitting
        function used (i.e. Taylor, etc), the fitted function name (i.e. logX, etc.)
        and the time variable for which this parametrization was computed.
        """
        with open(self.filepaths['fit']) as f:
            info = f.readline().strip().split()

        fit_function_name = info[1]
        fitted_function_filename = info[8][:-1]  # Remove trailing .
        time_variable = info[6]

        return fit_function_name, fitted_function_filename, time_variable

    def _find_model_title(self):
        preroot = self.preroot

        if 'eft' in preroot:
            return r'$V_0 (\xi_1 (\epsilon_F \phi)^2 + \xi_2 (\epsilon_F \phi)^ 4 + \sum_i \xi_i (\epsilon_F \phi)^i)$'
        elif 'monomial' in preroot:
            return r'$V_0 \phi^n$'
        elif 'tracker' in preroot:
            return r'$V_0 \phi^{-n}e^{\phi^m}$'
        elif 'axion' in preroot:
            return r'$V_0 \left[ 1 + \cos(\epsilon_F \phi) + \sum_{n=2}^{n_{max}}\xi_n\epsilon_{NP}^{n-1}\cos\left(n\epsilon_{NP}\phi\right)\right]$'
        elif 'modulus' in preroot:
            return r'$V_0 \left[ \sum_{n=0}^{n_{max}}\xi_n\epsilon_{D}^{n} e^{\alpha(p_D - n)\phi}\right]$'
        else:
            return ''

    def _get_fitted_function_label(self, fitted_function_name):
        """
        Return the fitted_function TeX-label
        """
        if 'logX' in fitted_function_name:
            fitted_function_label = r'$log(\rho_{\phi}/\rho_{\phi}(z=0))$'
        elif 'X' in fitted_function_name:
            fitted_function_label = r'$\rho_{\phi}/\rho_{\phi}(z=0)$'
        elif 'logRho' in fitted_function_name:
            fitted_function_label = r'$log(\rho_{\phi})$'
        elif 'w' in fitted_function_name:
            fitted_function_label = r'$w$'
        elif 'F' in fitted_function_name:
            fitted_function_label = r'$\int_0^{ln(a)}d\ln(a) w$'

        return fitted_function_label

    def _get_time_array(self):
        """
        Returns the time array for the variable used when fitting.
        """
        z = self.z

        if (self.time_variable == 'log(1+z)') or (self.time_variable == 'ln(1+z)'):
            time_array = np.log(1+z)
        elif self.time_variable == '(1-a)':
            time_array = z/(1+z)
        elif self.time_variable == 'lna':
            time_array = -np.log(1+z)
        elif self.time_variable == 'a':
            time_array = 1/(1+z)
        else:
            allowed_xvars = ['log(1+z)', '(1-a)', 'lna', 'a']
            raise ValueError('The time_variable {} is not implemented. Implemented time_variables are: {}'.format(self.time_variable, allowed_xvars))

        return time_array

    def _find_modelname(self):
        modelnameTMP = self.preroot.rstrip('/').split('/')[-1].split('-')
        modelnameTMP = modelnameTMP[:modelnameTMP.index('fit')]
        return '-'.join(modelnameTMP)

    def _save_computation(self, array, name, asis=False):
        outfile = os.path.join(self.filepaths['outdir'], name)
        if asis:
            np.savetxt(outfile + '.txt', np.array(array))
        else:
            np.savetxt(outfile + '.txt', np.array(array).T)

    def _save_H_DA_f_redshift_to_compare(self, obs, name, sampled):
        suffix = ''
        if sampled:
            suffix = self.suffix_sampled
        for i, z in enumerate(self.redshift_to_compare):
            fname = name + '-z_{}{}'.format(z, suffix)
            self._save_computation(np.array(obs)[:, i], fname)

    def _file_exists(self, name):
        outfile = os.path.join(self.filepaths['outdir'], name + '.txt')
        return os.path.isfile(outfile)

    def _read_file(self, name):
        outfile = os.path.join(self.filepaths['outdir'], name + '.txt')
        return np.loadtxt(outfile, unpack=True)

    # def _read_mean_std_or_compute_and_store(self, name, reldev):
    #     if self._file_exists(name):
    #         means, std = self._read_file(name)
    #         upper_limmit = means + std
    #     else:
    #         for maxreldevs in reldev:
    #             maxreldevs[maxreldevs == 0] = min(maxreldevs[maxreldevs != 0]) * 1e-20
    #         means = np.nanmean(np.log10(reldev), axis=1)
    #         std = np.nanstd(np.log10(reldev), axis=1)
    #         upper_limmit = means + std

    #         if self.save_final_computations:
    #             self._save_computation(np.array([means, std]), name)
    #     return upper_limmit

    def calculate_dimension(self):
        if self.basis_cut and (not self.analytical):
            selection = []
            if not len(self.obs_reldev['max']):
                self.compute_relative_error_vs_dimensions_observables()

            for reldevs, reldevs_DArec in zip(self.obs_reldev['max'],
                                              self.obs_reldev['DA_rec']):
                selection.append( (( sum(reldevs > self.basis_cut) /
                                    float(len(reldevs)) ) > self.acceptance) or
                                 (( sum(reldevs_DArec > self.basis_cut_DArec) /
                                    float(len(reldevs)) ) > self.acceptance) )

            if not np.all(selection):
                selection[sum(selection)] = True
            sys.stdout.write('Selected {} out of {} dimensions. Error in observables lower than {}.\n'.format(sum(selection), len(selection), self.basis_cut))
        else:
            selection = [True] * len(self.hist_orig_scale_orig.data)
            sys.stdout.write('Selected {} out of {} dimensions.\n'.format(sum(selection), len(selection)))
        self.selection = selection
        self.dim_red = sum(selection) - len(self.hist_orig_scale_orig.data)
        if not self.analytical:
            self.suffix_sampled += '-{}-coeffs'.format(sum(selection))

    def _get_residuals_hist(self):
        residualsHist = self.residualsHist
        residualsHist.read_file(self.filepaths['fit_reldev'])
        for i in range(1, len(residualsHist.data), 2):
            # odd indexes => reldevs
            # even indexes => z_max (and z_rec)
            residualsHist.data[i] = np.log10(residualsHist.data[i])

    def _get_hist_orig_scale_orig(self, hack_mono=False):
        hist = self.hist_orig_scale_orig

        hist.read_file(self.filepaths['fit'], header=self.header_row)

        if hist.data == []:
            sys.exit('Error with filepath: {}'.format(self.filepaths['fit']))

        if (self.hist_orig_scale_orig.data.shape[0] == 1) and hack_mono:
            data = self.hist_orig_scale_orig.data[0]
            self.hist_orig_scale_orig.data = np.array([data, np.zeros(data.size)])

        hist.compute(self.nbins)
        hist.compute_correlations()

    def _get_hist_corrected_scale_orig(self):
        hist = self.hist_corrected_scale_orig

        if self.histlogcomplex:
            data = wicmath.log_complex(self.hist_orig_scale_orig.data)
            hist.data = np.concatenate([data.real, data.imag])
            hist.bins = ['log10({}^{{+}})'.format(i) for i in self.hist_orig_scale_orig.bins]
            hist.bins.extend(['i logC({}^{{-}})'.format(i) for i in self.hist_orig_scale_orig.bins])
        elif self.histtransform:
            hist.data = self._transform_function(self.hist_orig_scale_orig.data)
            hist.bins = ['{}({})'.format(self._transform_function_label, i) for i in self.hist_orig_scale_orig.bins]
        else:
            hist = self.hist_corrected_scale_orig = self.hist_orig_scale_orig

        hist.compute_correlations()

    def _get_hist_diagonal_orig(self):
        """
        Change basis to covariance matrix diagonal basis
        """
        eigenValues, eigenVectors = np.linalg.eigh(self.hist_corrected_scale_orig.covariance)
        # Sort eigenValues decreasingly
        idx = np.abs(eigenValues).argsort()[::-1]
        self.eigenValues = eigenValues[idx]
        self.eigenVectors = eigenVectors[:, idx]

        sys.stdout.write('Eigenvalues: {}\n'.format(self.eigenValues))

        self.transfMatrix = self.eigenVectors.T
        sys.stdout.write('Transformation matrix: {}\n'.format(self.transfMatrix))

        self.diagonal_basis = self.transfMatrix.dot(self._raw_basis)

        hist_diagonal_orig = self.hist_diagonal_orig

        ################################################################################
        # Since we diagonlize the covariance matrix: TDT^t = C, C = <(x - <x>)(x-<x>)^t>
        # what we are transforming is, actually, T(x - <x>).
        #
        # Using Tx would require addition of T_truncated^t <Tx> when going back to
        # original basis.
        # <Tx (Tx)^t> =/= D --> Using Tc would not uncorrelate coeffs!!!
        ################################################################################

        self.hist_corrected_scale_means = np.mean(self.hist_corrected_scale_orig.data, axis=1)[:, None]
        hist_diagonal_orig.data = self.transfMatrix.dot(self.hist_corrected_scale_orig.data
                                                        - np.mean(self.hist_corrected_scale_orig.data, axis=1)[:, None])


        hist_diagonal_orig.bins = [r'D_{{{}}}'.format(i) for i in range(len(eigenValues))]

        hist_diagonal_orig.compute(self.nbins)

        hist_diagonal_orig.compute_covariance_matrix()

        if self.histlogauto:
            # Use percentage of data.
            for densities in np.array(self.hist_diagonal_orig.histograms)[:, 0]:
                if np.any(densities/np.sum(densities, dtype=float) > self.log_hist_threshold):
                    self._hist_diagonal_sampled_scale.append('log')
                else:
                    self._hist_diagonal_sampled_scale.append('linear')

    def _get_hist_corrected_scale_orig_reduced(self):
        """
        Check reduce basis -> original basis tranformation
        """
        if not len(self.selection):
            self.calculate_dimension()

        eigenValues = self.eigenValues

        backData = self.transfMatrix[self.selection].T.dot(self.hist_diagonal_orig.data[self.selection])
        backData += np.mean(self.hist_corrected_scale_orig.data, axis=1)[:, None]

        hist_corrected_scale_orig_reduced = self.hist_corrected_scale_orig_reduced
        hist_corrected_scale_orig_reduced.data = backData
        hist_corrected_scale_orig_reduced.bins = self.hist_corrected_scale_orig.bins
        hist_corrected_scale_orig_reduced.compute(self.nbins)
        hist_corrected_scale_orig_reduced.compute_correlations()

    def _get_hist_diagonal_sampled(self):
        name = 'coefficients_sampled'
        suffix = self.suffix_sampled
        data = []
        if self.histlogauto and self._hist_diagonal_sampled_scale:
            if not self._file_exists(name + suffix):
                for i, dat in enumerate(self.hist_diagonal_orig.data[self.selection]):
                    if self._hist_diagonal_sampled_scale[i] == 'log':
                        data.append(wicmath.sample_log_data(dat, self.nbins))
                    else:
                        data.append(self._sample_lin_data([self.hist_diagonal_orig.histograms[i]]))

                data = np.array(data)
        elif self.histlog:
            if not self._file_exists(name + suffix):
                for dat in self.hist_diagonal_orig.data[self.selection]:
                    data.append(wicmath.sample_log_data(dat, self.nbins))
        else:
            if not self._file_exists(name + suffix):
                data = self._sample_lin_data(np.array(self.hist_diagonal_orig.histograms)[self.selection])

        if data == []:
            data = self._read_file(name + suffix)
            if len(np.shape(data)) == 1:
                data = np.reshape(data, (1, -1))
        elif self.save_final_computations:
            self._save_computation(data, name+suffix)

        sys.stdout.write("{}\n".format(np.shape(data)))

        self.hist_diagonal_sampled.data = data
        self.hist_diagonal_sampled.bins = self.hist_diagonal_orig.bins[:sum(self.selection)]
        self.hist_diagonal_sampled.compute_covariance_matrix()

    def _get_hist_corrected_scale_sampled(self):
        hist_corrected_scale_sampled = self.hist_corrected_scale_sampled
        hist_corrected_scale_sampled.bins = self.hist_corrected_scale_orig.bins
        hist_corrected_scale_sampled.data = self.hist_corrected_scale_means + \
            self.transfMatrix[self.selection].T.dot(self.hist_diagonal_sampled.data)

    def _get_hist_orig_scale_sampled(self):
        hist = self.hist_orig_scale_sampled
        if self.analytical:
            if not os.path.isfile(self.filepaths['analytical']):
                raise ValueError('Analytical is True but there is not: ' + self.filepaths['analytical'])
            hist.read_file(self.filepaths['analytical'], header=self.header_row) # If txt.
            if (hist.data.shape[0] == 1):  # If we have get so far, hack_mono was set true before.
                hist.data = np.array([hist.data[0], np.zeros(hist.data[0].size)])
            if hist.data.shape[0] != self.hist_orig_scale_orig.data.shape[0]:
                raise ValueError('Analytical file must have same number of params as fit file')
            if hist.data.shape[1] < self.points_in_H_DA_f_plot:
                raise ValueError('Requested points_in_H_DA_f_plot is {}. You need at least the same number of sampled coeffs.'.format(self.points_in_H_DA_f_plot))

        else:
            data = self.hist_corrected_scale_sampled.data
            if self.histlogcomplex:
                data_reals = data[:len(data)/2]
                data_imag = data[len(data)/2:]
                hist.data = wicmath.log_complex_inverse(data_reals + data_imag * 1j)
                hist.bins = self.hist_orig_scale_orig.bins
            elif self.histtransform:
                hist.data = self._transform_function_inverse(data)
                hist.bins = self.hist_orig_scale_orig.bins
            else:
                hist = self.hist_orig_scale_sampled = self.hist_corrected_scale_sampled

        if self.remove_out_of_bounds:
            self._remove_out_of_bounds()

        hist.compute(self.nbins)

    def _get_hist_orig_scale_orig_reduced(self):
        hist = self.hist_orig_scale_orig_reduced
        data = self.hist_corrected_scale_orig_reduced.data
        if self.histlogcomplex:
            data_reals = data[:len(data)/2]
            data_imag = data[len(data)/2:]
            hist.data = wicmath.log_complex_inverse(data_reals + data_imag * 1j)
            hist.bins = self.hist_orig_scale_orig.bins
        elif self.histtransform:
            hist.data = self._transform_function_inverse(data)
            hist.bins = self.hist_orig_scale_orig.bins
        else:
            hist = self.hist_orig_scale_orig_reduced = self.hist_corrected_scale_orig_reduced
        hist.compute(self.nbins)

    def _get_hist_corrected_scale_rb_orig(self):
        # data_rf = wicmath.reflect_data(hist_orig_scale_orig.data)
        data_rf = self.hist_corrected_scale_orig.data

        hist_corrected_scale_rb_orig = self.hist_corrected_scale_rb_orig

        # Rebin data s.t. cov = 1.

        data_x = [dat+1 for dat in data_rf]   # w + 1
        hist_corrected_scale_rb_orig.data = data_x/np.sqrt(np.var(data_x, axis=1))[:, np.newaxis]

        hist_corrected_scale_rb_orig.bins = self.hist_corrected_scale_orig.bins

    def compute_coeffs_reduced(self):
        hist_orig_scale_orig = self.hist_orig_scale_orig

        selection = [False]*len(hist_orig_scale_orig.data)

        coeffs_reduced = []
        for i in range(len(hist_orig_scale_orig.data)):
            selection[i] = True

            corrected_scale_orig_reduced_data = \
            np.mean(self.hist_corrected_scale_orig.data, axis=1)[:, None] + \
                self.transfMatrix[selection].T.dot(self.hist_diagonal_orig.data[selection])

            coeffs_reduced.append(corrected_scale_orig_reduced_data)

        self.coeffs_reduced = np.array(coeffs_reduced)

    def compute_fitted_functions(self):
        self.fitted_functions = np.array([self._fit_function(self.time_array,
                                                             params) for params
                                          in
                                          zip(*self.hist_orig_scale_orig.data)])

    def compute_fitted_functions_sampled(self):
        self.fitted_functions_sampled = \
            np.array([self._fit_function(self.time_array, params) for params in
                      zip(*self.hist_orig_scale_sampled.data)])

    def compute_fitted_functions_reduced(self):
        if len(self.coeffs_reduced) == 0:
            self.compute_coeffs_reduced()

        _fit_function = self._fit_function
        self.fitted_functions_reduced = []

        for data in self.coeffs_reduced:
            if self.histlogcomplex:
                data_reals = data[:len(data)/2]
                data_imag = data[len(data)/2:]
                orig_scale_reduced_data = wicmath.log_complex_inverse(data_reals + data_imag * 1j)
            elif self.histtransform:
                orig_scale_reduced_data = self._transform_function_inverse(data)
            else:
                orig_scale_reduced_data = data

            Fint_red_tmp = np.array([_fit_function(self.time_array, params) for
                                     params in zip(*orig_scale_reduced_data)])

            self.fitted_functions_reduced.append(Fint_red_tmp)

    def compute_H_DA_f(self):
        # if not len(self.fitted_functions):
        #     self.compute_fitted_functions()
        # if not len(self.w):
        #     self.compute_w()

        # H, DA, f, z_rec, DA_rec = \
        #     self._get_H_DA_f(self.fitted_functions[:self.points_in_H_DA_f_plot],
        #                      self.w[:self.points_in_H_DA_f_plot],
        #                      self.redshift_reldevs,
        #                      original=True)

        if len(self.obs['z_rec']) and len(self.obs_to_compare['H']):
            sys.stdout.write('Already computed CLASS observables\n')
            return

        H, DA, f, z_rec, DA_rec = \
            self._get_H_DA_f_class(self.class_params[:self.points_in_H_DA_f_plot])

        i = self.redshift_to_compare_indices
        obs = H[:,i].T, DA[:,i].T, f[:,i].T

        self.obs_to_compare['H'], self.obs_to_compare['DA'], self.obs_to_compare['f'] = \
            obs

        self.obs['H'] = H
        self.obs['DA'] = DA
        self.obs['f'] = f
        self.obs['z_rec'] = z_rec
        self.obs['DA_rec'] = DA_rec

        errors = np.isnan(z_rec)
        if np.any(errors):
            esum = errors.sum()
            epct = esum / float(z_rec.size) * 100.
            warnings.warn('There were {} errors (i.e. {} % of computed models)'.format(esum, epct))

        if self.save_final_computations:
            name = 'H_DA_f_redshift_to_compare'
            self._save_H_DA_f_redshift_to_compare(obs, name, False)
            name = 'zDArec'
            self._save_computation(np.concatenate([z_rec, DA_rec], axis=1), name, asis=True)

    def compute_H_DA_f_sampled(self):
        if len(self.obs_sampled['z_rec'] and len(self.obs_sampled_to_compare['H'])):
            sys.stdout.write('Already computed sampled observables\n')
            return

        if self.use_class:
            H, DA, f, z_rec, DA_rec = \
                self._get_H_DA_f_class(self.class_params_analytical[:self.points_in_H_DA_f_plot], f0orig=False)
        else:
            if not len(self.fitted_functions_sampled):
                self.compute_fitted_functions_sampled()
            if not len(self.w_sampled):
                self.compute_w_sampled()


            H, DA, f, z_rec, DA_rec = \
                self._get_H_DA_f(self.fitted_functions_sampled[:self.points_in_H_DA_f_plot],
                                 self.w_sampled[:self.points_in_H_DA_f_plot],
                                 self.redshift_reldevs,
                                 original=False)

        self.obs_sampled['H'] = H
        self.obs_sampled['DA'] = DA
        self.obs_sampled['f'] = f
        self.obs_sampled['z_rec'] = z_rec   # Should be fixed to np.mean(self.obs['z_rec'])
        self.obs_sampled['DA_rec'] = DA_rec

        i = self.redshift_to_compare_indices
        obs = H[:,i].T, DA[:,i].T, f[:,i].T
        self.obs_sampled_to_compare['H'], self.obs_sampled_to_compare['DA'], self.obs_sampled_to_compare['f'] = \
            obs

        if self.save_final_computations:
            name = 'H_DA_f_redshift_to_compare-sampled'
            self._save_H_DA_f_redshift_to_compare(obs, name, True)

            name = 'zDArec-sampled'
            self._save_computation(np.concatenate([z_rec, DA_rec], axis=1), name, True)

    def compute_H_DA_f_redshift_to_compare(self):
        name = 'H_DA_f_redshift_to_compare'
        names = []
        for z in self.redshift_to_compare:
            names.append(name + '-z_{}'.format(z))

        names.append('zDArec')

        if np.all(map(self._file_exists, names)):
            H_ar, DA_ar, f_ar = [], [], []
            for i, z in enumerate(self.redshift_to_compare):
                H, DA, f = self._read_file(names[i])
                H_ar.append(H)
                DA_ar.append(DA)
                f_ar.append(f)
            self.obs_to_compare['H'] = np.array(H_ar)
            self.obs_to_compare['DA'] = np.array(DA_ar)
            self.obs_to_compare['f'] = np.array(f_ar)

            zrec, DArec = self._read_file(names[-1])
            self.obs_to_compare['z_rec'] = zrec
            self.obs_to_compare['DA_rec'] = DArec

            errors = np.isnan(zrec)
            if np.any(errors):
                esum = errors.sum()
                epct = esum / float(zrec.size) * 100.
                warnings.warn('There were {} errors (i.e. {} % of computed models)'.format(esum, epct))
        else:
            self.compute_H_DA_f()

    def compute_H_DA_f_redshift_to_compare_sampled(self):
        name = 'H_DA_f_redshift_to_compare-sampled'
        names = []
        for z in self.redshift_to_compare:
            names.append(name + '-z_{}{}'.format(z, self.suffix_sampled))

        names.append('zDArec-sampled')

        if np.all(map(self._file_exists, names)):
            H_ar, DA_ar, f_ar = [], [], []
            for i, z in enumerate(self.redshift_to_compare):
                H, DA, f = self._read_file(names[i])
                H_ar.append(H)
                DA_ar.append(DA)
                f_ar.append(f)
            self.obs_sampled_to_compare['H'] = np.array(H_ar)
            self.obs_sampled_to_compare['DA'] = np.array(DA_ar)
            self.obs_sampled_to_compare['f'] = np.array(f_ar)

            zrec, DArec = self._read_file(names[-1])
            self.obs_sampled_to_compare['z_rec'] = zrec
            self.obs_sampled_to_compare['DA_rec'] = DArec
        else:
            self.compute_H_DA_f_sampled()

    def compute_H_DA_f_reduced(self):
        if not self.use_class:
            if not len(self.fitted_functions_reduced):
                self.compute_fitted_functions_reduced()

            if not len(self.w_reduced):
                self.compute_w_reduced()

        if len(self.coeffs_reduced) == 0:
            self.compute_coeffs_reduced()

        for i in range(len(self.coeffs_reduced)):
            if self.use_class:
                class_params = [self.class_params_parametrization(coeffs, self.class_params[imodel])
                                for imodel, coeffs in enumerate(self.coeffs_reduced[i, :, :self.points_in_H_DA_f_plot].T)]

                H, DA, f, z_rec, DA_rec = \
                    self._get_H_DA_f_class(class_params, f0orig=False)
            else:
                functions = self.fitted_functions_reduced[i]
                w_red = self.w_reduced[i]
                H, DA, f, z_rec, DA_rec = \
                    self._get_H_DA_f(functions[:self.points_in_H_DA_f_plot],
                                     w_red[:self.points_in_H_DA_f_plot],
                                     self.redshift_reldevs,
                                     original=True)

            self.obs_reduced['H'].append(H)
            self.obs_reduced['DA'].append(DA)
            self.obs_reduced['DA_rec'].append(DA_rec)
            self.obs_reduced['f'].append(f)

    def compute_H_DA_f_reduced_to_compare_with_exact(self):
        if len(self.obs_reldev['DA_rec']):
            sys.stdout.write('Already computed observable relative difference for reduced funtion\n')
            return

        if not self.use_class:
            if not len(self.fitted_functions_reduced):
                self.compute_fitted_functions_reduced()

            if not len(self.w_reduced):
                self.compute_w_reduced()

        H_ar, DA_ar, DA_ar_rec, f_ar = [], [], [], []

        H_to_compare_ar, DA_to_compare_ar, DArec_to_compare_ar, f_to_compare_ar, zrec_to_compare_ar = [], [], [], [], []

        if len(self.coeffs_reduced) == 0:
            self.compute_coeffs_reduced()

        for i in range(len(self.coeffs_reduced)):
            if self.use_class:
                class_params = [self.class_params_parametrization(coeffs, self.class_params[imodel])
                                for imodel, coeffs in enumerate(self.coeffs_reduced[i, :, :self.points_in_H_DA_f_plot].T)]

                H, DA, f, z_rec, DA_rec = \
                    self._get_H_DA_f_class(class_params, f0orig=False)
            else:
                functions = self.fitted_functions_reduced[i]
                w_red = self.w_reduced[i]
                H, DA, f, z_rec, DA_rec = \
                    self._get_H_DA_f(functions[:self.points_in_H_DA_f_plot],
                                     w_red[:self.points_in_H_DA_f_plot],
                                     self.redshift_reldevs,
                                     original=True)
            # Remove the reldevs at recombination index [0]. Leave it just for DA_rec
            H_ar.append(_maxreldev(self.obs['H'], H))
            DA_ar.append(_maxreldev(self.obs['DA'], DA))
            DA_ar_rec.append(_maxreldev(self.obs['DA_rec'], DA_rec))
            f_ar.append(_maxreldev(self.obs['f'], f))

            i = self.redshift_to_compare_indices
            H_to_compare_ar.append(H[:, i].T)
            DA_to_compare_ar.append(DA[:, i].T)
            f_to_compare_ar.append(f[:, i].T)
            DArec_to_compare_ar.append(DA_rec)
            zrec_to_compare_ar.append(z_rec)



        self.obs_to_compare_reduced['H'] = np.array(H_to_compare_ar)
        self.obs_to_compare_reduced['DA'] = np.array(DA_to_compare_ar)
        self.obs_to_compare_reduced['f'] = np.array(f_to_compare_ar)
        self.obs_to_compare_reduced['DA_rec'] = np.array(DArec_to_compare_ar)
        self.obs_to_compare_reduced['z_rec'] = np.array(zrec_to_compare_ar)

        self.obs_reldev['H'] = np.array(H_ar)
        self.obs_reldev['DA'] = np.array(DA_ar)
        self.obs_reldev['DA_rec'] = np.array(DA_ar_rec)
        self.obs_reldev['f'] = np.array(f_ar)

        if self.save_final_computations:
            for i in range(len(H_ar)):
                name = 'H_DA_f_redshift_to_compare_reduced_{}_coeff'.format(i+1)
                obs = self.obs_to_compare_reduced['H'][i],\
                      self.obs_to_compare_reduced['DA'][i],\
                      self.obs_to_compare_reduced['f'][i]
                self._save_H_DA_f_redshift_to_compare(obs, name, False)

                name = 'zDArec'
                self._save_computation(np.concatenate([self.obs_to_compare_reduced['z_rec'][i],
                                                       self.obs_to_compare_reduced['DA_rec'][i]],
                                                      axis=1), name, asis=True)

    def compute_relative_error_vs_dimensions_fitted_function(self):
        name_reldev = 'relative-error-dimensions-fitted-function'

        if self._file_exists(name_reldev):
            reldev = self._read_file(name_reldev)
        else:
            if not len(self.fitted_functions):
                self.compute_fitted_functions()
            if not len(self.fitted_functions_reduced):
                self.compute_fitted_functions_reduced()

            reldev = _maxreldev_dimensions(self.fitted_functions,
                                              self.fitted_functions_reduced)

            if self.save_final_computations:
                self._save_computation(reldev, name_reldev)

        self.relative_errors_vs_dimmensions_fitted_function = reldev

    def compute_relative_error_vs_dimensions_observables(self):
        name_reldev = 'relative-error-dimensions-'
        name_reldev_H = name_reldev + 'H'
        name_reldev_DA = name_reldev + 'DA'
        name_reldev_DA_rec = name_reldev + 'DA_rec'
        name_reldev_f = name_reldev + 'f'
        name_reldev_obs = name_reldev + 'observables'

        names_reldev = [name_reldev_H, name_reldev_DA, name_reldev_f]

        if np.all(map(self._file_exists, names_reldev)):
                self.obs_reldev['H'] = \
                    self._read_file(name_reldev_H)
                self.obs_reldev['DA'] = \
                    self._read_file(name_reldev_DA)
                self.obs_reldev['DA_rec'] = \
                    self._read_file(name_reldev_DA_rec)
                self.obs_reldev['f'] = \
                    self._read_file(name_reldev_f)
        else:
            if not len(self.obs['H']):
                self.compute_H_DA_f()

            if not len(self.obs_reduced['H']):
                if self.store_intermediate_computations:
                    self.compute_H_DA_f_reduced()

                    # Remove the reldevs at recombination. Leave it just for DA_rec
                    self.obs_reldev['H'] = \
                        _maxreldev_dimensions(self.obs['H'], self.obs_reduced['H'])
                    self.obs_reldev['DA'] = \
                         _maxreldev_dimensions(self.obs['DA'], self.obs_reduced['DA'])
                    self.obs_reldev['DA_rec'] = \
                        _maxreldev_dimensions(self.obs['DA_rec'], self.obs_reduced['DA_rec'])
                    self.obs_reldev['f'] = \
                        _maxreldev_dimensions(self.obs['f'], self.obs_reduced['f'])
                else:
                    self.compute_H_DA_f_reduced_to_compare_with_exact()

            # if not self.store_intermediate_computations:
            #     self._empty_intermediate_computations()

            if self.save_final_computations:
                self._save_computation(self.obs_reldev['H'], name_reldev_H)
                self._save_computation(self.obs_reldev['DA'], name_reldev_DA)
                self._save_computation(self.obs_reldev['DA_rec'], name_reldev_DA_rec)
                self._save_computation(self.obs_reldev['f'], name_reldev_f)

        if self._file_exists(name_reldev_obs):
            reldev = self._read_file(name_reldev_obs)
        else:
            reldev = []
            for rd_H, rd_DA, rd_f in zip(self.obs_reldev['H'],
                                          self.obs_reldev['DA'],
                                          self.obs_reldev['f']):
                reldev.append(np.max([rd_H, rd_DA, rd_f], axis=0))
            reldev = np.array(reldev)
            if self.save_final_computations:
                self._save_computation(reldev, name_reldev_obs)

        self.obs_reldev['max'] = reldev

    def compute_w(self):
        self.w = self._get_w(self.fitted_functions)

    def compute_w_reduced(self):
        for functions in self.fitted_functions_reduced:
            w = self._get_w(functions)
            self.w_reduced.append(w)

    def compute_w_sampled(self):
        self.w_sampled = self._get_w(self.fitted_functions_sampled)

    def _empty_intermediate_computations(self):
        self.fitted_functions_reduced = []
        self.w_reduced = []
        # self.H = []
        # self.DA = []
        # self.f = []
        self.obs_reduced = self.obs_empty.copy()
        self.obs_sampled = self.obs_empty.copy()

    def _get_w(self, fitted_function):
        lna = self.lna
        fitted_function_filename = self.fitted_function_filename

        if fitted_function_filename in ['logX', 'logRho']:
            ThreewPlus1 = np.gradient(fitted_function, lna, edge_order=2,
                                      axis=1)
            w = ThreewPlus1/3. - 1
        elif fitted_function_filename == 'X':
            #TODO: WHY NOT LOG(X)!?
            logX = np.log(fitted_function)
            ThreewPlus1 = np.gradient(logX, lna, edge_order=2, axis=1)
            # This is an approximation but should work well if function is
            # dense enough
            w = ThreewPlus1/3. - 1
        elif fitted_function_filename == 'w':
            w = fitted_function
        elif fitted_function_filename == 'F':
            w = np.gradient(fitted_function, -lna, edge_order=2, axis=1)

        return np.array(w)

    def _get_w0wa_fit(self, z, rhos, w_fit, obs):
        rhoM_ar, rhoR_ar, rhoDE_fit_ar = rhos
        H, DA, f = obs

        def iterator():
            for arr in zip(rhoM_ar, rhoR_ar, rhoDE_fit_ar, H, DA, f, w_fit):
                yield z, arr

        it = iterator()

        sys.stdout.write('Computing w0wa_fitted...\n')
        if self.multiprocess:
            with MPIPoolExecutor() as executor:
                w0fit, wafit = zip(*executor.map(_compute_w0wa_fit_from_array_wrapper, it, chunksize=self.chunksize))
        else:
            w0fit, wafit = zip(*map(_compute_w0wa_fit_from_array_wrapper, it))

        return np.array(w0fit), np.array(wafit)

    def _get_rhos(self, sampled_realizations, original=False):
        z = self.z
        ldist = len(sampled_realizations)  # self.points_in_H_DA_f_plot

        name = 'original-rho0s'
        if self._file_exists(name):
            rho0_m, rho0_rad, rho0_DE = self._read_file(name)
        else:
            def iterator():
                for d in self.class_params[:ldist]:
                    pars = d.copy()
                    del(pars['output'])
                    del(pars['z_max_pk'])
                    yield pars

            arr = iterator()

            if self.multiprocess:
                with MPIPoolExecutor() as executor:
                     output = list(executor.map(_compute_rhos_from_class, arr,
                                                chunksize=self.chunksize))
            else:
                 output = list(map(_compute_rhos_from_class, arr,
                                   chunksize=self.chunksize))

            rho0_m, rho0_rad, rho0_DE = np.array(zip(*output))
            self._save_computation(np.stack([rho0_m, rho0_rad,
                                                 rho0_DE]), name)
        if not original:
            # rho0_m = rho0_m[~np.isnan(rho0_m)]
            # rho0_m = np.min(rho0_m) * \
            #     stats.rv_histogram(np.histogram(rho0_m/np.min(rho0_m),
            #                                     bins=self.nbins)).rvs(size=ldist)
            # rho0_DE = rho0_m[~np.isnan(rho0_DE)]
            # rho0_DE = np.min(rho0_DE) * \
            #     stats.rv_histogram(np.histogram(rho0_DE/np.min(rho0_DE), bins=self.nbins)).rvs(size=ldist)

            # BETTER RESULTS WITH THIS METHOD
            #### We would have to change these if they are changed in computation.py ####
            h = np.random.uniform(0.6, 0.8, ldist)  # self.points_in_H_DA_f_plot)
            Omega0_cdm = np.random.uniform(0.15, 0.35, ldist)  #, self.points_in_H_DA_f_plot)

            ########################################
            # CLASS constants and definitions
            # include/background.h
            _Mpc_over_m_ = 3.085677581282e22
            _k_B_ = 1.3806504e-23
            _h_P_ = 6.62606896e-34
            _c_ = 2.99792458e8
            _G_ = 6.67428e-11
            # source/input.c
            sigma_B = 2. * pow(np.pi, 5) * pow(_k_B_, 4) / 15. / pow(_h_P_, 3) / pow(_c_, 2)
            H0 = h * 1.e5 / _c_
            T_cmb = 2.7255
            Omega0_g = (4.*sigma_B/_c_*pow(T_cmb, 4.)) / (3.*_c_*_c_*1e10*h**2./_Mpc_over_m_/_Mpc_over_m_/8./np.pi/_G_)
            Omega0_ur = 3.046*7./8.*pow(4./11., 4./3.)*Omega0_g
            Omega0_b = 0.022032/pow(h, 2)
            ########################################
            Omega0_m = Omega0_cdm + Omega0_b
            Omega0_rad = Omega0_g + Omega0_ur
            rho0_DE = H0**2. * (1 - Omega0_m - Omega0_rad)  # Without multiplying by 3, to use CLASS convention
            rho0_m = H0**2. * Omega0_m                      # Without multiplying by 3, to use CLASS convention
            rho0_rad = H0**2. * Omega0_rad                  # Without multiplying by 3, to use CLASS convention

        rhoM = rho0_m[:, None] * (1 + z)**3
        rhoR = rho0_rad[:, None] * (1 + z)**4

        if self.fitted_function_filename == 'logX':
            rhoDE = rho0_DE[:ldist, None] * np.exp(sampled_realizations)
        elif self.fitted_function_filename == 'X':
            rhoDE = rho0_DE[:ldist, None] * sampled_realizations
        elif self.fitted_function_filename == 'F':
            rhoDE = rho0_DE[:ldist, None] * (1 + z)**3 * np.exp(-3 * sampled_realizations)
        elif self.fitted_function_filename == 'w':
            rhoDE = np.empty((ldist, z.size))
            for i, yfit1 in enumerate(sampled_realizations):
                Fint = co.compute_F(z, yfit1)
                rhoDE[i] = rho0_DE[i] * np.exp(-3 * Fint) * (1+z)**3

        return rhoM, rhoR, rhoDE

    def _get_H_DA_f(self, sampled_realizations, w_ar, redshifts_indexes, original=False):
        rhoM_ar, rhoR_ar, rhoDE_fit_ar = self._get_rhos(sampled_realizations, original)

        if not len(self.obs_to_compare['z_rec']):
            self.compute_H_DA_f_redshift_to_compare()
        z_rec_ar = self.obs_to_compare['z_rec']  # shape is (nmodels, )

        z_max = self.z[redshifts_indexes][0]
        if z_max not in self.redshift_to_compare:
            if len(self.obs['f']) == 0:
                self.compute_H_DA_f()
            f0_ar = np.array([interp1d(self.z[self.redshift_reldevs], f)(z_max) for f in self.obs['f']])
        else:
            sel = np.where(z_max == self.redshift_to_compare)[0][0]
            f0_ar = self.obs_to_compare['f'][sel]  # shape is (z_to_compare, nmodels)


        if not original:
            ldist = self.hist_orig_scale_sampled.data.shape[1]
            z_rec_hist = stats.rv_histogram(np.histogram(z_rec_ar[~np.isnan(z_rec_ar)], bins=self.nbins))
            z_rec_ar = z_rec_hist.rvs(size=ldist)
            f0_hist = stats.rv_histogram(np.histogram(f0_ar[~np.isnan(f0_ar)], bins=self.nbins))
            f0_ar = f0_hist.rvs(size=ldist)

        def iterator():
            i = 0
            for arr in zip(rhoM_ar, rhoR_ar, rhoDE_fit_ar, w_ar):
                f0 = f0_ar[i]
                z_rec = z_rec_ar[i]
                i += 1
                yield self.z, arr, redshifts_indexes, f0, z_rec

        arr = iterator()

        sys.stdout.write('Computing observables...\n')

        if self.multiprocess:
            with MPIPoolExecutor() as executor:
                obs, rec = zip(*list(executor.map(_compute_H_DA_f_from_array_wrapper, arr, chunksize=self.chunksize)))
        else:
           obs, rec = zip(*map(_compute_H_DA_f_from_array_wrapper, arr))

        H, DA, f  = np.array(zip(*obs))
        z_rec, DA_rec = np.array(zip(*rec))
        z_rec = z_rec.reshape(-1, 1)
        DA_rec = DA_rec.reshape(-1, 1)

        return H, DA, f, z_rec, DA_rec

    def _get_H_DA_f_class(self, params, f0orig=False):
        z_output = self.z[self.redshift_reldevs]
        z_max = z_output[0]

        if f0orig:
            if self.redshift_to_compare[0] != z_max: # Sorted by decreasing ordered
                self.compute_H_DA_f()
                f0_ar = self.obs['f'][:, 0]
            elif len(self.obs_to_compare['f']) == 0:
                self.compute_H_DA_f_redshift_to_compare()
                f0_ar = self.obs_to_compare['f'][0]  # Sorted by decreasing ordered
            else:
                f0_ar = self.obs_to_compare['f'][0]  # Sorted by decreasing ordered
            ldist = len(params)
            f0_hist = stats.rv_histogram(np.histogram(f0_ar[~np.isnan(f0_ar)], bins=self.nbins))
            f0_ar = f0_hist.rvs(size=ldist)

        def iterator():
            for i, pars in enumerate(params):
                if f0orig:
                    yield pars, self.z_low_redshift_fit, z_output, f0_ar[i], self.fclass
                else:
                    yield pars, self.z_low_redshift_fit, z_output, None, self.fclass

        arr = iterator()

        sys.stdout.write('Computing observables from CLASS...\n')

        if self.multiprocess:
            with MPIPoolExecutor() as executor:
                results = list(executor.map(_compute_H_DA_f_from_class_wrapper, arr, chunksize=self.chunksize))
        else:
           results = list(map(_compute_H_DA_f_from_class_wrapper, arr))

        obs, rec, rhos = zip(*results)
        H, DA, f  = np.array(zip(*obs))
        z_rec, DA_rec = np.array(zip(*rec))
        z_rec = z_rec.reshape(-1, 1)
        DA_rec = DA_rec.reshape(-1, 1)

        name = 'original-rho0s'
        self._save_computation(rhos, name, asis=True)

        return H, DA, f, z_rec, DA_rec

    def _get_raw_basis(self):
        num_dim = len(self.hist_orig_scale_orig.data)
        coeffs = np.identity(num_dim)
        return np.array([self._fit_function(self.time_array, c) for c in coeffs])

    def _fit_function():
        pass

    def _select_crazy_functions():
        pass

    def _transform_function():
        pass

    def _transform_function_inverse():
        pass

    def _remove_out_of_bounds(self):
        hist = self.hist_orig_scale_sampled

        orig_data = self.hist_orig_scale_orig.data

        selection = np.prod((hist.data > np.min(orig_data, axis=1)[:,None]) &
                            (hist.data < np.max(orig_data, axis=1)[:,None]),
                            axis=0, dtype=bool)

        self.percentage_ofb_removed = 100. * (~selection).sum(dtype=float)/len(selection)

        d = []
        for data in hist.data:
            d.append(data[selection])

        hist.data = d

        sys.stdout.write("Removed out-of-bounds models in orig scale; i.e. {:.2f}% of data\n".format(self.percentage_ofb_removed))

    def _remove_outliers(self, hist_orig_scale_orig):
        """
        Remove outliers from histogram data further than
        self.remove_outliers_sigma * sigma
        """
        mean = np.mean(hist_orig_scale_orig.data, axis=1)[:,None]
        std = np.std(hist_orig_scale_orig.data, axis=1)[:,None]
        ns = self.remove_outliers_sigma
        selection = np.prod((hist_orig_scale_orig.data >= mean - ns*std) & (hist_orig_scale_orig.data <=
                                                            mean + ns*std),
                            axis=0, dtype=bool)

        self.percentage_outliers_removed = 100. * (~selection).sum(dtype=float)/len(selection)

        data = []
        for i in range(len(hist_orig_scale_orig.data)):
            data.append(hist_orig_scale_orig.data[i][selection])

        hist_orig_scale_orig.data = data

        sys.stdout.write("Removed outliers at > {} sigma; i.e. {:.2f}% of data\n".format(self.remove_outliers_sigma, self.percentage_outliers_removed))

    def _sample_lin_data(self, hists):
            data = []
            #for i in np.array(hist_diagonal_orig.histograms)[selection]:
            #    h_d = stats.rv_histogram(i)
            #    dat2 = h_d.rvs(size=len(hist_orig_scale_orig.data[0]))
            #    #np.random.shuffle(dat2)
            #    data.append(dat2)
            h_d = []
            for i in hists:
                h_d.append(stats.rv_histogram(i))
            for run in range(len(self.hist_orig_scale_orig.data[0])):
                data_run = []
                for h in h_d:
                    data_run.append(h.rvs())
                data.append(data_run)

            data = np.array(data).flatten()

            return data

    def set_fit_function(self, f):
        if callable(f):
            self._fit_function = f
        elif 'Taylor_legacy_c0' == f:
            self._fit_function = wicmath.Taylor_legacy_c0
        elif 'Taylor_c0' == f:
            self._fit_function = wicmath.Taylor_c0
        elif 'Taylor_legacy_c1' == f:
            self._fit_function = wicmath.Taylor_legacy_c1
        elif 'Taylor_c1' == f:
            self._fit_function = wicmath.Taylor_c1
        elif 'Taylor_legacy' == f:
            self._fit_function = wicmath.Taylor_legacy
        elif 'Taylor_log' == f:
            self._fit_function = wicmath.Taylor_log
        elif 'Taylor' == f:
            self._fit_function = wicmath.Taylor
        elif 'bins' == f:
            self._fit_function = wicmath.bins_equally_spaced
        elif 'Pade' == f:
            self._fit_function = wicmath.Pade
        else:
            raise ValueError('fit_function ({}) is not a function nor any of the implemented cases'.format(f))

        if type(f) is str:
            sys.stdout.write('Fit function detected is ' + f + '\n')

    def set_select_crazy_functions(self, f):
        self._select_crazy_functions = f
        self.remove_crazy_functions = True

    def set_transform_function(self, f, finv, label='f'):
        self._transform_function = f
        self._transform_function_inverse = finv
        self._transform_function_label = label
        self.histtransform = True

    def prepare_histograms(self, run_residuals=True,
                           only_residuals=False,
                           run_check_dim_reduction_sampling=True,
                           run_rebinned_histograms=True, hack_mono=False):


        if run_check_dim_reduction_sampling and self.analytical:
            raise ValueError('run_check_dim_sampling and analytical cannot be set True simultaneosly (at least yet)')

        ####### Residuals hists #######
        if run_residuals:
            self._get_residuals_hist()

        if only_residuals:
            return

        self._get_hist_orig_scale_orig(hack_mono)

        self._raw_basis = self._get_raw_basis()

        self._get_hist_corrected_scale_orig()

        # TODO: Remove this?
        # if self.remove_outliers_sigma:
        #     self._remove_outliers(hist_orig_scale_orig)

        #######

        ######
        # Change basis to covariance matrix diagonal basis
        ######
        self._get_hist_diagonal_orig()

        #####
        # Check reduce basis -> original basis tranformation
        #####
        self._get_hist_corrected_scale_orig_reduced()
        self._get_hist_orig_scale_orig_reduced()

        ########### Check sampling #############
        if run_check_dim_reduction_sampling:
            self._get_hist_diagonal_sampled()
            self._get_hist_corrected_scale_sampled()
            self._get_hist_orig_scale_sampled()

        if self.analytical:
            self._get_hist_orig_scale_sampled()


        ########### Rebinning ############
        if run_rebinned_histograms:
            self._get_hist_corrected_scale_rb_orig()

    def compute_fitted_functions_original_and_sampled(self):
        if not len(self.fitted_functions):
            self.compute_fitted_functions()

        if not len(self.fitted_functions_sampled):
            self.compute_fitted_functions_sampled()

        if self.remove_crazy_functions:
            f = self.fitted_functions_sampled
            # selection = ~np.any(np.diff(np.sign(f)) != 0, axis=1)
            # selection = ~np.any((np.abs(f) > self.crazyness) != 0, axis=1)
            selection = ~self._select_crazy_functions(f)
            self.fitted_functions_sampled = f[selection]
            self.percentage_remove_crazy_functions = 100. * (1. - selection.sum() / float(len(f)))
            sys.stdout.write('Removed crazy functions ({}% computed)\n'.format(self.percentage_remove_crazy_functions))

    def compute_w_original_and_sampled(self):
        if not len(self.w):
            self.compute_w()

        if not len(self.w_sampled):
            self.compute_w_sampled()

    def compute_w0wa_fit_original_and_sampled(self, zmax=2.):
        name_w0wa_fit = 'w0wa_fit-zmax-{}'.format(zmax)
        name_w0wa_fit_sampled = 'w0wa_fit_sampled-zmax-{}{}'.format(zmax, self.suffix_sampled)

        ######################################################################
        ######################################################################
        ######################################################################
        ########################### w0wa from original fit ###################
        ######################################################################

        if self._file_exists(name_w0wa_fit):
            self.hist_w0wa_fit.data = self._read_file(name_w0wa_fit)
        else:
            self.hist_w0wa_fit.data = self.compute_w0wa_fit_original(zmax)

            if self.save_final_computations:
                self._save_computation(self.hist_w0wa_fit.data, name_w0wa_fit)

        ######################################################################
        ######################################################################
        ######################################################################
        ########################### w0wa from sampled ########################
        ######################################################################

        if self._file_exists(name_w0wa_fit_sampled):
            self.hist_w0wa_fit_sampled.data = self._read_file(name_w0wa_fit_sampled)
        else:
            self.hist_w0wa_fit_sampled.data = self.compute_w0wa_fit_sampled(zmax)

            if self.save_final_computations:
                self._save_computation(self.hist_w0wa_fit_sampled.data, name_w0wa_fit_sampled)

    def compute_H_DA_f_original_and_sampled(self):
        if not len(self.obs_to_compare['H']):
            self.compute_H_DA_f()

        if not len(self.obs_sampled_to_compare['H']):
            self.compute_H_DA_f_sampled()

    def compute_w0wa_fit_original(self, zmax, index=-1):
        z = self.z
        if not len(self.fitted_functions):
            self.compute_fitted_functions()
        fitted_functions = self.fitted_functions[:self.points_in_H_DA_f_plot]

        if not len(self.w_reduced):
            w_fit = self._get_w(fitted_functions)
        else:
            w_fit = self.w_reduced[index][:self.points_in_H_DA_f_plot]

        if not len(self.obs_reduced['H']):
            H, DA, f, _, _= self._get_H_DA_f(fitted_functions, w_fit,
                                             z<=zmax, original=True)
            obs = (H, DA, f)
        else:
            zobs = z[z <= self.z_low_redshift_fit]
            obs = (self.obs_reduced['H'][index][:self.points_in_H_DA_f_plot][:, zobs<=zmax],
                   self.obs_reduced['DA'][index][:self.points_in_H_DA_f_plot][:, zobs<=zmax],
                   self.obs_reduced['f'][index][:self.points_in_H_DA_f_plot][:, zobs<=zmax])

        rhos = self._get_rhos(fitted_functions, original=True)

        return self._get_w0wa_fit(z[z<=zmax], np.asarray(rhos)[:, :, z<=zmax],
                                                     w_fit[:, z<=zmax], obs)

    def compute_w0wa_fit_sampled(self, zmax):
        z = self.z
        if not len(self.fitted_functions_sampled):
            self.compute_fitted_functions_sampled()
        fitted_functions = self.fitted_functions_sampled[:self.points_in_H_DA_f_plot]

        if not len(self.w_sampled):
            w_fit = self._get_w(fitted_functions)
        else:
            w_fit = self.w_sampled[:self.points_in_H_DA_f_plot]

        if not len(self.obs_sampled['H']):
            H, DA, f, _, _= self._get_H_DA_f(fitted_functions, w_fit,
                                             z<=zmax, original=False)
            obs = (H, DA, f)
        else:
            zobs = z[z <= self.z_low_redshift_fit]
            obs = (self.obs_sampled['H'][:self.points_in_H_DA_f_plot][:, zobs<=zmax],
                   self.obs_sampled['DA'][:self.points_in_H_DA_f_plot][:, zobs<=zmax],
                   self.obs_sampled['f'][:self.points_in_H_DA_f_plot][:, zobs<=zmax])

        rhos = self._get_rhos(fitted_functions, original=False)

        return self._get_w0wa_fit(z[z<=zmax], np.asarray(rhos)[:, :, z<=zmax],
                                                     w_fit[:, z<=zmax], obs)

