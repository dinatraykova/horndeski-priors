#!/bin/bash
#SBATCH --job-name=HP-plot
#SBATCH -o log/plot-fig/HP-plot-%J.out
#SBATCH -e log/plot-fig/HP-plot-%J.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=carlosgarcia@iff.csic.es
#SBATCH -n 20
#SBATCH --cpus-per-task=1
#SBATCH --time=1-00:00:00
#SBATCH --mem=15GB
#SBATCH	-p special

############################################################

export PATH="/home/iff/cggarcia/codes/anaconda2/bin:$PATH"

HOMEDIR=/home/iff/cggarcia/
Python=$HOMEDIR/codes/anaconda2/bin/python
working_path=$HOMEDIR/hordenski_priors/binning/
codes_path=$working_path/codes

cd $codes_path

export I_MPI_FABRICS=shm:tcp

# mpiexec.hydra -bootstrap slurm $Python -m mpi4py.futures $codes_path/plot-figures.py $working_path/$1
mpiexec.hydra -bootstrap slurm $Python -m mpi4py.futures $codes_path/compute_analysis_results.py $working_path/$1

exit
